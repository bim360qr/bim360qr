﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace QR_Versions
{
    class Program
    {
        static string CLIENT_ID = "pDpxh0pbktWj4lvr8yDmJ2J7VZA2Qkkp";
        static string CLIENT_SECRET = "pF14xBtMvjaAUamR";
        static Token t = Token.Get("data:read data:write", CLIENT_ID, CLIENT_SECRET);

        static void Main(string[] args)
        {
            string url = "https://developer.api.autodesk.com/data/v1/projects/b.7471a1a6-0326-4c30-a396-b3e0481b0539/versions/urn:adsk.wipprod:fs.file:vf.IuDt0ZZRQj-MfVRIm2aAJQ%3Fversion=2";
            var ver = Version.Get(url);
            int versionId = ver.data.attributes.versionNumber;
            int versionLast = 0;
            string uid = ver.data.relationships.item.data.id;
            var versions = Versions.Get("https://developer.api.autodesk.com/data/v1/projects/b.7471a1a6-0326-4c30-a396-b3e0481b0539/items/" + uid + "/versions");
            foreach (var data in versions.data)
            {
                if (data.attributes.versionNumber > versionLast) versionLast = data.attributes.versionNumber;
            }
            Console.WriteLine("Input: "+url);
            Console.WriteLine("Current version: " + versionId);
            Console.WriteLine("Newest version: " + versionLast);
            if (versionId == versionLast)
                Console.Write("You are using latest version");
            else
                Console.Write("You are using expired version");
            Console.Read();
        }
        public static string getByURL(string url)
        {
            using (var webClient = new WebClient())
            {
                webClient.Headers.Add("Authorization", t.token_type + ' ' + t.access_token);
                var pars = new NameValueCollection();
                var response = webClient.DownloadData(url);
                return System.Text.Encoding.UTF8.GetString(response);
            }
        }

        public static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
