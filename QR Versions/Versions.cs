﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace QR_Versions
{
    [DataContract]
    public class Versions
    {
        public static DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Versions));
        [DataMember] public List<Data> data { get; set; }

        static public Versions Get(string url)
        {
            var str = Program.getByURL(url);
            Versions PS;
            using (var stream = Program.GenerateStreamFromString(str))
            {
                PS = (Versions)ser.ReadObject(stream);
            }
            return PS;
        }

        [DataContract]
        public class Data
        {
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
            [DataMember] public Attributes attributes { get; set; }
            [DataMember] public Links links { get; set; }
        }
        [DataContract]
        public class Links
        {
            [DataMember] public Self self { get; set; }
        }
        [DataContract]
        public class Self
        {
            [DataMember] public string href { get; set; }
        }

        public class Attributes
        {
            public string name { get; set; }
            public string displayName { get; set; }
            public string createUserId { get; set; }
            public string createUserName { get; set; }
            public string lastModifiedUserId { get; set; }
            public string lastModifiedUserName { get; set; }
            public int versionNumber { get; set; }
            public string fileType { get; set; }
        }

    }

}

