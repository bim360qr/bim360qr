﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace PDFModifier
{
    [DataContract]
    public partial class Projects
    {

        public static Projects Get(string account)
        {
            string responce = Program.getByURL("https://developer.api.autodesk.com/project/v1/hubs/b."+account+"/projects");
            using (var stream = Program.GenerateStreamFromString(responce))
            {
                return (Projects)ser.ReadObject(stream);
            }
        }

        [DataMember]
        public Jsonapi jsonapi { get; set; }
        [DataMember]
        public Links links { get; set; }
        [DataMember]
        public List<Datum> data { get; set; }

        public static DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Projects));
        [DataContract]
        public class Jsonapi
        {
            [DataMember]
            public string version { get; set; }
        }

        [DataContract]
        public class Self
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Links
        {
            [DataMember]
            public Self self { get; set; }
        }

        [DataContract]
        public class Schema
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Data
        {
        }

        [DataContract]
        public class Extension
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string version { get; set; }
            [DataMember]
            public Schema schema { get; set; }
            [DataMember]
            public Data data { get; set; }
        }

        [DataContract]
        public class Attributes
        {
            [DataMember]
            public string name { get; set; }
            [DataMember]
            public Extension extension { get; set; }
        }

        [DataContract]
        public class Self2
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Links2
        {
            [DataMember]
            public Self2 self { get; set; }
        }

        [DataContract]
        public class Data2
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string id { get; set; }
        }

        [DataContract]
        public class Related
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Links3
        {
            [DataMember]
            public Related related { get; set; }
        }

        [DataContract]
        public class Hub
        {
            [DataMember]
            public Data2 data { get; set; }
            [DataMember]
            public Links3 links { get; set; }
        }

        [DataContract]
        public class Data3
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string id { get; set; }
        }

        [DataContract]
        public class Link
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Meta
        {
            [DataMember]
            public Link link { get; set; }
        }

        [DataContract]
        public class RootFolder
        {
            [DataMember]
            public Data3 data { get; set; }
            [DataMember]
            public Meta meta { get; set; }
        }

        [DataContract]
        public class Related2
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Links4
        {
            [DataMember]
            public Related2 related { get; set; }
        }

        [DataContract]
        public class TopFolders
        {
            [DataMember]
            public Links4 links { get; set; }
        }

        [DataContract]
        public class Data4
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string id { get; set; }
        }

        [DataContract]
        public class Link2
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Meta2
        {
            [DataMember]
            public Link2 link { get; set; }
        }

        [DataContract]
        public class Issues
        {
            [DataMember]
            public Data4 data { get; set; }
            [DataMember]
            public Meta2 meta { get; set; }
        }

        [DataContract]
        public class Data5
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string id { get; set; }
        }

        [DataContract]
        public class Link3
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Meta3
        {
            [DataMember]
            public Link3 link { get; set; }
        }

        [DataContract]
        public class Submittals
        {
            [DataMember]
            public Data5 data { get; set; }
            [DataMember]
            public Meta3 meta { get; set; }
        }

        [DataContract]
        public class Data6
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string id { get; set; }
        }

        [DataContract]
        public class Link4
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Meta4
        {
            [DataMember]
            public Link4 link { get; set; }
        }

        [DataContract]
        public class Rfis
        {
            [DataMember]
            public Data6 data { get; set; }
            [DataMember]
            public Meta4 meta { get; set; }
        }

        [DataContract]
        public class Data7
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string id { get; set; }
        }

        [DataContract]
        public class Link5
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Meta5
        {
            [DataMember]
            public Link5 link { get; set; }
        }

        [DataContract]
        public class Markups
        {
            [DataMember]
            public Data7 data { get; set; }
            [DataMember]
            public Meta5 meta { get; set; }
        }

        [DataContract]
        public class Data8
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string id { get; set; }
        }

        [DataContract]
        public class Link6
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Meta6
        {
            [DataMember]
            public Link6 link { get; set; }
        }

        [DataContract]
        public class Checklists
        {
            [DataMember]
            public Data8 data { get; set; }
            [DataMember]
            public Meta6 meta { get; set; }
        }

        [DataContract]
        public class Data9
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string id { get; set; }
        }

        [DataContract]
        public class Link7
        {
            [DataMember]
            public string href { get; set; }
        }

        [DataContract]
        public class Meta7
        {
            [DataMember]
            public Link7 link { get; set; }
        }

        [DataContract]
        public class Cost
        {
            [DataMember]
            public Data9 data { get; set; }
            [DataMember]
            public Meta7 meta { get; set; }
        }

        [DataContract]
        public class Relationships
        {
            [DataMember]
            public Hub hub { get; set; }
            [DataMember]
            public RootFolder rootFolder { get; set; }
            [DataMember]
            public TopFolders topFolders { get; set; }
            [DataMember]
            public Issues issues { get; set; }
            [DataMember]
            public Submittals submittals { get; set; }
            [DataMember]
            public Rfis rfis { get; set; }
            [DataMember]
            public Markups markups { get; set; }
            [DataMember]
            public Checklists checklists { get; set; }
            [DataMember]
            public Cost cost { get; set; }
        }

        [DataContract]
        public class Datum
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string id { get; set; }
            [DataMember]
            public Attributes attributes { get; set; }
            [DataMember]
            public Links2 links { get; set; }
            [DataMember]
            public Relationships relationships { get; set; }
        }

    }

}
