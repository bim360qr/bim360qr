﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
//using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZXing;
using ZXing.Common;

namespace PDFModifier
{
    class Program
    {
        static Socket socketForClient;
        static StreamWriter writer;
        static StreamReader reader;
        static void socketListen()
        {
            TcpListener listener = new TcpListener(IPAddress.Loopback, 10585);
            listener.Start();
            socketForClient = listener.AcceptSocket();
            NetworkStream stream = new NetworkStream(socketForClient);
            writer = new StreamWriter(stream);
            reader = new StreamReader(stream);

        }
        public static Token token;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static void checkToken()
        {
            if (token != null && token.endTime > DateTime.Now) return;
            token = Token.Get("data:create data:read data:write", param["FORGE_CLIENT_ID"], param["FORGE_CLIENT_SECRET"]);
        }
        static Dictionary<string, string> param;
        static bool readConfig()
        {
            if (File.Exists("PDFModifier.cfg"))
            {
                string cfgRaw;
                using (var SW = new StreamReader(File.OpenRead("PDFModifier.cfg")))
                {
                    cfgRaw = SW.ReadToEnd().Replace("\r", "");
                }
                string[] cfg = cfgRaw.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in cfg)
                {
                    var pair = line.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (pair.Length != 2)
                        throw new Exception("Incorrect line: " + line);
                    foreach (var p in param.Keys)
                        if (pair[0].ToUpper() == p)
                        {
                            param[p] = pair[1];
                            break;
                        }
                }
                foreach (var p in param.Keys)
                    if (param[p] == null)
                    {
                        Console.WriteLine("Missing parameter: " + p);
                        return false;
                    }
                return true;
            }
            else
            {
                using (var SW = new StreamWriter(File.OpenWrite("PDFModifier.cfg")))
                {
                    foreach (var p in param.Keys)
                        SW.WriteLine(p + ':');
                }
                Console.WriteLine("new config file created in folder:\n" + Directory.GetCurrentDirectory());
                return false;
            }
        }

        static void Main(string[] args)
        {
            param = new Dictionary<string, string>();
            param.Add("ACCOUNT_ID", null);
            param.Add("PROJECT_ID", null);
            param.Add("FORGE_CLIENT_ID", null);
            param.Add("FORGE_CLIENT_SECRET", null);
            param.Add("PATH", null);
            string file_id = null;
            string folder_id = null;
            Folder f=null;
            Projects.Datum project = null;
            string pdfName = null;
            {
                Console.WriteLine("Reading config");
                if (!readConfig())
                    return;
                bool skip = false;
                string qrLink = "";
                if (!skip)
                {
                    Console.WriteLine("Loading projects from account: " + param["ACCOUNT_ID"]);
                    var projects = Projects.Get(param["ACCOUNT_ID"]);
                    Console.WriteLine("Searching for project: " + param["PROJECT_ID"]);
                    Thread.Sleep(500);
                    foreach (var d in projects.data)
                        if (d.id.Contains(param["PROJECT_ID"]))
                        {
                            project = d;
                            Console.WriteLine("Found project: " + d.id);
                            Thread.Sleep(500);
                            break;
                        }
                    if (project == null)
                        throw new Exception("Project " + param["PROJECT_ID"] + " not found");
                    projects = null;
                    Console.WriteLine("Loading root folder content");
                    Thread.Sleep(500);
                    folder_id = project.relationships.rootFolder.data.id;
                    f = Folder.Get(project.relationships.rootFolder.meta.link.href + "/contents");
                    string[] path = param["PATH"].Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                    pdfName = path[path.Length - 1];
                    Console.WriteLine("Reading folders: ");
                    Thread.Sleep(500);
                    for (int i = 0; i < path.Length - 1; i++)
                    {
                        f = f.Navigate(path[i]);
                        Console.Write(path[i] + "\\");
                        Thread.Sleep(500);
                    }
                    Versions vers = null;
                    foreach (var d in f.data)
                        if (d.attributes.displayName == path[path.Length - 1])
                        {
                            Console.WriteLine(path[path.Length - 1]);
                            Console.WriteLine("Loading versions");
                            Thread.Sleep(500);
                            vers = Versions.Get(d.links.self.href + "/versions");
                            file_id = d.id;
                            break;
                        }
                    if (vers == null)
                        throw new Exception("File " + path[path.Length - 1] + " not found");
                    byte[] fileBytes = null;
                    Console.WriteLine("Downloading file");
                    Thread.Sleep(500);
                    fileBytes = Program.getBytesURL(vers.data[0].relationships.storage.meta.link.href);
                    qrLink = vers.data[0].links.self.href;
                    int version = int.Parse(qrLink.Substring(qrLink.LastIndexOf('=') + 1));
                    qrLink = qrLink.Substring(0, qrLink.LastIndexOf('=') + 1);
                    qrLink = qrLink + (version + 1);
                    if (File.Exists("tmpPDFFile"))
                        File.Delete("tmpPDFFile");
                    using (var SW = (File.OpenWrite("tmpPDFFile")))
                    {
                        SW.Write(fileBytes, 0, fileBytes.Length);
                    }
                }

                string mask = "projects/";
                qrLink = qrLink.Substring(qrLink.IndexOf(mask) + mask.Length) + ';' +
                    param["FORGE_CLIENT_ID"] + ';' +
                   param["FORGE_CLIENT_SECRET"];

                Console.WriteLine("Updating file");
                Thread.Sleep(500);
                int imagesReplaced = 0;
                if (File.Exists(pdfName))
                    File.Delete(pdfName);
                using (PdfReader pdf = new PdfReader("tmpPDFFile"))
                {
                    PdfStamper stp = new PdfStamper(pdf, new FileStream(pdfName, FileMode.Create));
                    PdfWriter writer = stp.Writer;
                    Bitmap imgb = null;
                    if (qrLink == "")
                    {
                        imgb = new Bitmap(100, 100);
                        for (int i = 0; i < imgb.Width; i++)
                            for (int j = 0; j < imgb.Width; j++)
                            {
                                var color = Color.Black;
                                if (i == 0 || j == 0 || i == imgb.Width - 1 || j == imgb.Width - 1)
                                    color = Color.Blue;
                                imgb.SetPixel(i, j, color);
                            }
                    }
                    else
                    {
                        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                        BitMatrix bitMatrix = multiFormatWriter.encode(qrLink, BarcodeFormat.QR_CODE, 500, 500);
                        ZXing.BarcodeWriter barcodeEncoder = new BarcodeWriter();
                        imgb = barcodeEncoder.Write(bitMatrix);
                    }
                    imgb.Save("image.png");
                    int cnt = 0;
                    for (int i = 1; i <= pdf.NumberOfPages; i++)
                    {
                        PdfDictionary pg = pdf.GetPageN(i);
                        PdfDictionary res =
                          (PdfDictionary)PdfReader.GetPdfObject(pg.Get(PdfName.RESOURCES));
                        PdfDictionary xobj =
                          (PdfDictionary)PdfReader.GetPdfObject(res.Get(PdfName.XOBJECT));
                        if (xobj != null)
                        {
                            foreach (PdfName name in xobj.Keys)
                            {
                                PdfObject obj = xobj.Get(name);
                                if (obj.IsIndirect() && PdfReader.GetPdfObject(obj) is PdfDictionary)
                                {

                                    PdfDictionary tg = (PdfDictionary)PdfReader.GetPdfObject(obj);
                                    PdfName type =
                                      (PdfName)PdfReader.GetPdfObject(tg.Get(PdfName.SUBTYPE));
                                    if (PdfName.IMAGE.Equals(type))
                                    {
                                        byte[] bytes = PdfReader.GetStreamBytesRaw((PRStream)PdfReader.GetPdfObject(obj));
                                        if ((bytes != null))
                                        {
                                            using (var SW = (File.OpenWrite("tmpimg.png")))
                                            {
                                                SW.Write(bytes, 0, bytes.Length);
                                            }
                                            try
                                            {
                                                System.Drawing.Image imgPDF = System.Drawing.Image.FromFile("tmpimg.png");
                                                var bitmap = new Bitmap(imgPDF);
                                                var code = r.Decode(bitmap);
                                                bitmap.Dispose();
                                                imgPDF.Dispose();
                                                if (code != null && code.Text == @"https://www.pss.spb.ru/")
                                                {
                                                    PdfReader.KillIndirect(obj);
                                                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance("image.png");
                                                    iTextSharp.text.Image maskImage = img.ImageMask;
                                                    if (maskImage != null)
                                                        writer.AddDirectImageSimple(maskImage);
                                                    writer.AddDirectImageSimple(img, (PRIndirectReference)obj);
                                                    imagesReplaced++;
                                                }
                                            }
                                            catch { }

                                        }

                                    }
                                }
                            }
                        }
                    }
                    Console.WriteLine("Codes replaced: " + imagesReplaced);
                    Thread.Sleep(500);
                    try { File.Delete("image.png"); }
                    catch { }
                    try { File.Delete("tmpimg.png"); }
                    catch { }
                    stp.Close();
                }
                if (imagesReplaced > 0)
                {
                    Console.WriteLine("Uploading file");
                    Thread.Sleep(500);
                    f.UploadFile(project.id, f.id, pdfName, file_id);
                }
                else
                {
                    Console.WriteLine("File is already updated");
                }
                if (File.Exists("tmpPDFFile"))
                    try
                    {
                        File.Delete("tmpPDFFile");
                        Console.WriteLine("Temp file removed");
                        Thread.Sleep(500);
                    }
                    catch
                    {
                        Console.WriteLine("Failed to remove temp file");
                        Thread.Sleep(500);
                    }
                if (File.Exists(pdfName))
                    try
                    {
                        File.Delete(pdfName);
                        Console.WriteLine("Updated file removed");
                        Thread.Sleep(500);
                    }
                    catch
                    {
                        Console.WriteLine("Failed to remove updated file");
                        Thread.Sleep(500);
                    }
                Console.WriteLine("Program done");
                Thread.Sleep(2000);
            }
            //catch (Exception ex) { Console.Write(ex.Message); Thread.Sleep(5000); return; }
            /*
            using (Process proc = Process.Start(start))
            {

                Console.WriteLine("Waiting for connection..");
                socketListen();
                Console.WriteLine("Connected");

                //writer.WriteLine(Convert.ToBase64String(fileBytes));
                //writer.Flush();
                //PdfDocument doc = new PdfDocument();
                //doc.LoadFromFile("test0.pdf");

                Console.WriteLine("Pages: " + doc.Pages.Count);
                for (int i = 0; i < doc.Pages.Count; i++)
                    foreach (var info in doc.Pages[i].ImagesInfo)
                    {
                        ZXing.BarcodeReader r = new BarcodeReader();
                        var img = info.Image;
                        var res= r.Decode(new Bitmap(info.Image));
                        if (res!=null && res.Text == @"https://www.pss.spb.ru/")
                        {
                            RectangleF rect = info.Bounds;
                            writer.WriteLine("{0};{1};{2};{3};{4};{5};{6}", i, rect.X, rect.Y, rect.Width, rect.Height, img.Width, img.Height);
                            writer.Flush();
                        }
                    }
                writer.WriteLine("end");
                writer.Flush();
                proc.WaitForExit();
            }
        */
        }
        static ZXing.BarcodeReader r = new BarcodeReader();
        public static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        public static Stream GenerateStreamFromString(byte[] s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        public static string getByURL(string url)
        {
            while (true)
            {
                try
                {
                    Program.checkToken();
                    using (var webClient = new WebClient())
                    {
                        webClient.Headers.Add("Authorization", token.token_type + ' ' + token.access_token);
                        return System.Text.Encoding.UTF8.GetString(webClient.DownloadData(url));
                    }
                }

                catch (Exception ex)
                {
                    if (ex.Message.Contains("401") || ex.Message.Contains("403") || ex.Message.Contains("404"))
                        throw new Exception("ACCOUNT_ID not valid");
                    Console.Title = "!" + DateTime.Now + "#" + ex.Message;
                    Thread.Sleep(1000);
                }
            }
        }
        public static byte[] getBytesURL(string url)
        {
            while (true)
            {
                try
                {
                    Program.checkToken();
                    using (var webClient = new WebClient())
                    {
                        webClient.Headers.Add("Authorization", token.token_type + ' ' + token.access_token);
                        return webClient.DownloadData(url);
                    }
                }

                catch (Exception ex)
                {
                    if (ex.Message.Contains("401") || ex.Message.Contains("403") || ex.Message.Contains("404"))
                        throw new Exception("ACCOUNT_ID not valid");
                    Console.Title = "!" + DateTime.Now + "#" + ex.Message;
                    Thread.Sleep(1000);
                }
            }
        }

    }
}
