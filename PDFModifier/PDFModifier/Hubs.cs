﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace PDFModifier
{
    [DataContract]
    public class Hubs
    {


        public static Hubs Get()
        {
            string responce = Program.getByURL("https://developer.api.autodesk.com/project/v1/hubs");
            using (var stream = Program.GenerateStreamFromString(responce))
            {
                return (Hubs)ser.ReadObject(stream);
            }
        }


        [DataMember(Name="data")]
        public Datum[] Data { get; set; }

        [DataMember(Name="jsonapi")]
        public Jsonapi jsonapi { get; set; }

        [DataMember(Name="links")]
        public DatumLinks Links { get; set; }

        public static DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Hubs));

        [DataContract]
        public partial class Datum
        {
            [DataMember(Name = "relationships")]
            public Relationships Relationships { get; set; }

            [DataMember(Name = "attributes")]
            public Attributes Attributes { get; set; }

            [DataMember(Name = "type")]
            public string Type { get; set; }

            [DataMember(Name = "id")]
            public string Id { get; set; }

            [DataMember(Name = "links")]
            public DatumLinks Links { get; set; }
        }
        [DataContract]
        public partial class Attributes
        {
            [DataMember(Name = "name")]
            public string Name { get; set; }

            [DataMember(Name = "extension")]
            public Extension Extension { get; set; }
        }
        [DataContract]
        public partial class Extension
        {

            [DataMember(Name = "version")]
            public string Version { get; set; }

            [DataMember(Name = "type")]
            public string Type { get; set; }

            [DataMember(Name = "schema")]
            public Self Schema { get; set; }
        }
        [DataContract]
        public partial class Self
        {
            [DataMember(Name = "href")]
            public string Href { get; set; }
        }
        [DataContract]
        public partial class DatumLinks
        {
            [DataMember(Name = "self")]
            public Self Self { get; set; }
        }
        [DataContract]
        public partial class Relationships
        {
            [DataMember(Name = "projects")]
            public Projects Projects { get; set; }
        }
        [DataContract]
        public partial class Projects
        {
            [DataMember(Name = "links")]
            public ProjectsLinks Links { get; set; }
        }
        [DataContract]
        public partial class ProjectsLinks
        {
            [DataMember(Name = "related")]
            public Self Related { get; set; }
        }
        [DataContract]
        public partial class Jsonapi
        {
            [DataMember(Name = "version")]
            public string Version { get; set; }
        }
    }
    
  
}
