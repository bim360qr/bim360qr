﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Setup_PSS_360VerScan_Actions
{
    public class CustomActions
    {
        [CustomAction]
        public static ActionResult WriteSerialNumber(Session session)
        {
            session.Log("Begin WriteSerialNumber");
            string serialnum = session["SERIAL_NUMBER"].ToUpper();
            session.Log(string.Format("CA WriteSerialNumber: S\\N {0}", serialnum));

            Regex regex = new Regex("^[A-F0-9]{8}-([A-F0-9]{4}-){3}[A-F0-9]{12}$");
            Match match = regex.Match(serialnum);

            if (match.Success)
            {
                session["CHECKSERIAL"] = "1";
                session["SERIAL_NUMBER"] = serialnum;
                return ActionResult.Success;
            }
            else
            {
                session["CHECKSERIAL"] = "0";
                MessageBox.Show("Введён некорректный серийный номер.\n" + serialnum, "PSS 360Backup", MessageBoxButtons.OK);
                return ActionResult.Success;
            }
        }
    }
}
