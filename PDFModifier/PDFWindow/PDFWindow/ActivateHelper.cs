﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Microsoft.Win32;
using System.Net.NetworkInformation;
using System.Net;
using System.IO;

namespace PSS_360VerScan
{
    public static class ActivateHelper
    {
        //private static string varDate;

        public static string getRegKey(string key)
        {
            string result = null;
            string key32 = null;
            string key64 = null;

            RegistryKey localmachine32 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
            RegistryKey localmachine64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);

            RegistryKey PSS_360VerScan_32;
            RegistryKey PSS_360VerScan_64;

            if (key == "AC")
            {
                PSS_360VerScan_32 = localmachine32.OpenSubKey(@"SOFTWARE\PSS GRAITEC\PSS 360VerScan\");
                PSS_360VerScan_64 = localmachine64.OpenSubKey(@"SOFTWARE\Wow6432Node\PSS GRAITEC\PSS 360VerScan");
            }
            //else if (key == "SN")
            //{
            //    PSS_360VerScan_32 = localmachine32.OpenSubKey(@"SOFTWARE\PSS GRAITEC\PSS 360VerScan\");
            //    PSS_360VerScan_64 = localmachine64.OpenSubKey(@"SOFTWARE\Wow6432Node\PSS GRAITEC\PSS 360VerScan");
            //}
            else
            {
                PSS_360VerScan_32 = localmachine32.OpenSubKey(@"SOFTWARE\PSS GRAITEC\PSS 360VerScan\");
                PSS_360VerScan_64 = localmachine64.OpenSubKey(@"SOFTWARE\Wow6432Node\PSS GRAITEC\PSS 360VerScan");
            }

            try
            {
                key32 = (string)PSS_360VerScan_32.GetValue(key);
                key64 = (string)PSS_360VerScan_64.GetValue(key);
            }
            catch (Exception ex) { }

            result = key32 == null ? key64 : key32;

            return result;
        }



        /// <summary>
        /// Finds the MAC address of the first operation NIC found.
        /// </summary>
        /// <returns>The MAC address.</returns>
        public static string getMacAddress()
        {
            string macAddresses = string.Empty;

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    macAddresses += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }

            return macAddresses;
        }
        

                       


        public static string getDatePlusYear()
        {
            DateTime date = GetDateTime().AddMonths(12);
            string Date = date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat);
            //string Date = date.ToShortDateString();
            return Date;
        }


        public static string getDateNow()
        {
            DateTime date = GetDateTime();
            string Date = date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat);
            //string Date = date.ToShortDateString();
            return Date;
        }


        public static DateTime GetDateTime(string str)
        {
            try
            {
                DateTime dateTime = DateTime.MinValue;
                System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://www.microsoft.com");
                request.Method = "GET";
                request.Accept = "text/html, application/xhtml+xml, */*";
                request.UserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)";
                request.ContentType = "application/x-www-form-urlencoded";
                request.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string todaysDates = response.Headers["date"];

                        dateTime = DateTime.ParseExact(todaysDates, "ddd, dd MMM yyyy HH:mm:ss 'GMT'",
                            System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, System.Globalization.DateTimeStyles.AssumeUniversal);
                    }
                }
                return dateTime;
            }
            catch (WebException wex)
            {
                string Error = "";
                try
                {
                    Error = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
                }
                catch
                {
                    Error = wex.Message;
                }

                if (Error.Contains("429") || Error.Contains("502") || Error.ToLower().Contains("истекло") || Error.ToLower().Contains("отмен") || Error.ToLower().Contains("прерван"))
                {
                    System.Threading.Thread.Sleep(1000);
                    return GetDateTime();
                }
                throw new Exception(wex.Message + ". Проверьте подключение к интернету.");
            }
        }

        public static DateTime GetDateTime()
        {
            try
            {
                DateTime dateTime = DateTime.MinValue;
                string uri = "https://developer.api.autodesk.com/authentication/v1/authenticate";
                System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                //request.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                //request.UseDefaultCredentials = true;

                using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string todaysDates = response.Headers["date"];

                        dateTime = DateTime.ParseExact(todaysDates, "ddd, dd MMM yyyy HH:mm:ss 'GMT'",
                            System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, System.Globalization.DateTimeStyles.AssumeUniversal);
                    }
                }
                return dateTime;
            }
            catch (WebException wex)
            {
                string Error = "";
                try
                {
                    Error = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();

                    try
                    {
                        var response = wex.Response;
                        string todaysDates = response.Headers["date"];
                        DateTime dateTime = DateTime.MinValue;
                        dateTime = DateTime.ParseExact(todaysDates, "ddd, dd MMM yyyy HH:mm:ss 'GMT'",
                            System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, System.Globalization.DateTimeStyles.AssumeUniversal);
                        return dateTime;
                    }
                    catch (Exception ex)
                    {

                    }
                }
                catch
                {
                    Error = wex.Message;
                }

                if (Error.Contains("429") || Error.Contains("502") || Error.ToLower().Contains("истекло") || Error.ToLower().Contains("отмен") || Error.ToLower().Contains("прерван"))
                {
                    System.Threading.Thread.Sleep(1000);
                    return GetDateTime();
                }
                throw new Exception(wex.Message + ". Проверьте подключение к интернету.");
            }
        }

    }

}





    