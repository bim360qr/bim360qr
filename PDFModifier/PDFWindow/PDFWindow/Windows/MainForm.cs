﻿using iTextSharp.text.pdf;
using PDFWindow;
using PDFWindow.BIM;
using PDFWindow.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZXing;
using ZXing.Common;
using NLog;
using NLog.Config;
using NLog.Targets;
using System.Security.Cryptography;
using PSS_360VerScan;
using PSS_360VerScan.Settings;

namespace PDFWindow
{
    struct TreeItem
    {
        public bool folder;
        public bool loaded;
        public string name;
        public string path;
    }
    public partial class MainForm : Form
    {
        public static object _ThreadLock = new object();
        public delegate void InvokeDelegate();
        TreeNode dummy;
        public static string CLIENT_ID = null;//"5yEW7BnyFgVP4ZPDOax7mMPEFAKoJ7K0";
        public static string CLIENT_SECRET = null;//"PbyiFpFaKPYQeshO";
        public static string ACCOUNT_ID = null;//"711f5cca-eae9-47de-ba5b-cd3ad6902d27";

        Parameters par = new Parameters();
        public static string cfgPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "PSS GRAITEC\\PSS 360VerScan\\main.cfg");

        public bool LoadConfig()
        {
            if (File.Exists(cfgPath))
            {
                try
                {
                    string[] values = Decrypt(File.ReadAllText(cfgPath)).Split(':');
                    CLIENT_ID = values[0];
                    CLIENT_SECRET = values[1];
                    ACCOUNT_ID = values[2];
                    return true;

                }
                catch {
                    try {
                        File.Delete(cfgPath);
                        MessageBox.Show(this, "Failed to load config", "Error");
                    }
                    catch
                    {
                        MessageBox.Show(this, "Config file is blocked", "Error");
                    }
            }
            }
            return false;
        }
        public void SaveConfig()
        {
            try
            {
                Directory.CreateDirectory(Directory.GetParent(cfgPath).FullName);
                using (StreamWriter SW = new StreamWriter(File.Create(cfgPath)))
                {
                    SW.Write(Encrypt($"{CLIENT_ID}:{CLIENT_SECRET}:{ACCOUNT_ID}"));
                }
            }
            catch { MessageBox.Show(this, "Failed to save config", "Error"); }
        }




        private static bool OnStartup()
        {
            //Program.log.Info("Проверка лицензии...");

            if (!Activated_app())
            {
                string APP_NAME = "PSS 360VerScan";
                ActivationDlg activation_dlg = new ActivationDlg(APP_NAME);
                activation_dlg.ShowDialog();
                if (activation_dlg.DialogResult != System.Windows.Forms.DialogResult.OK)
                {
                    string info = "Лицензия не активирована.\nНеобходимо ввести код активации в окне \"Активация\".\n" +
                    "Чтобы получить код активации, отправьте код запроса на helpme@pss.spb.ru.";

                    System.Windows.Forms.MessageBox.Show(info, "Предупреждение", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    string message = "Лицензия не активирована.";
                    {
                        //Program.log.Fatal(message + " Завершение работы приложения.");
                    }
                    return false;
                }
                else
                {
                    return true;
                }

            }
            //Program.log.Info("Успешная проверка лицензии.");
            return true;
        }

        private static bool Activated_app()
        {
            //bool result = false;
            string macAddress = ActivateHelper.getMacAddress();
            string ActivationCode = ActivateHelper.getRegKey("AC");
            string s_dateNow = ActivateHelper.getDateNow();
            string serialNumber = string.Empty;

#if DEMO_VERSION
            serialNumber = "DEMO_VERSION";
#elif DEMO_VERSION_60
            serialNumber = "DEMO_VERSION_60";
#elif BETA_VERSION
            serialNumber = "BETA_VERSION";
#else
            serialNumber = ActivateHelper.getRegKey("SN");
#endif


            string macAddressAndDate = "";
            if (ActivationCode != null)
            {

                try
                {
                    DSCryptor.CryptoRijndael decryptoRCtoMAC = new DSCryptor.CryptoRijndael("GoodDayTodayPSS360VerScan{C2ED9983-1BA9-44F0-A284-C28BC13CC802}");
                    macAddressAndDate = decryptoRCtoMAC.Decrypt(ActivationCode);
                }
                catch (Exception ex)
                {
                    //log.Error("Невозможно дешифровать ключи.");
                    return false;
                }
            }
            else return false;

            string[] mac_and_date = macAddressAndDate.Split('&');
            string s_reqDate;
            if (mac_and_date.Length == 2)
            {
                s_reqDate = mac_and_date[1];
            }
            else
            {
                //log.Error("Ошибка проверки кода активации. Невозможно дешифровать ключи.");
                return false;
            }


            DSCryptor.CryptoRijndael cryptoActKey = new DSCryptor.CryptoRijndael(serialNumber);
            string activationKey = cryptoActKey.Encrypt(macAddress);
            DSCryptor.CryptoRijndael cryptoActKey_a = new DSCryptor.CryptoRijndael("GoodDayTodayPSS360VerScan{C2ED9983-1BA9-44F0-A284-C28BC13CC802}");
            string activationCode = cryptoActKey_a.Encrypt(activationKey + '&' + s_reqDate);

            if (ActivationCode == activationCode)
            {
                DateTime dateNow = DateTime.ParseExact(s_dateNow, "dd/MM/yyyy",
                    System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, System.Globalization.DateTimeStyles.AssumeUniversal);

                DateTime reqDate = DateTime.ParseExact(s_reqDate, "dd/MM/yyyy",
                    System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, System.Globalization.DateTimeStyles.AssumeUniversal);

                if (dateNow.Date > reqDate.Date)
                {
                    //Program.log.Fatal("Срок лицензии истёк");
                    MessageBox.Show("Срок лицензии истёк", "Ошибка активации лицензии", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                return true;
            }
            else return false;
        }

        /*
        private static bool Activated_app()
        {
            //bool result = false;
            string macAddress = ActivateHelper.getMacAddress();
            string ActivationCode = ActivateHelper.getRegKey("AC");
            string s_dateNow = ActivateHelper.getDateNow();            


            string macAddressAndDate = "";
            if (ActivationCode != null)
            {

                try
                {
                    DSCryptor.CryptoRijndael decryptoRCtoMAC = new DSCryptor.CryptoRijndael("GoodDayTodayPSS360VerScan{C2ED9983-1BA9-44F0-A284-C28BC13CC802}");
                    macAddressAndDate = decryptoRCtoMAC.Decrypt(ActivationCode);
                }
                catch (Exception ex)
                {
                    //log.Error("Невозможно дешифровать ключи.");
                    return false;
                }
            }
            else return false;

            string[] max_and_date = macAddressAndDate.Split('&');
            string s_reqDate;
            if (max_and_date.Length == 2)
            {
                s_reqDate = max_and_date[1];
            }
            else
            {
                //log.Error("Невозможно дешифровать ключи.");
                return false;
            }

            DSCryptor.CryptoRijndael cryptoActKey = new DSCryptor.CryptoRijndael("GoodDayTodayPSS360VerScan{C2ED9983-1BA9-44F0-A284-C28BC13CC802}");
            string activationKey = cryptoActKey.Encrypt(macAddress + '&' + s_reqDate);

            if (ActivationCode == activationKey)
            {
                DateTime dateNow = DateTime.ParseExact(s_dateNow, "dd.MM.yyyy",
                    System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, System.Globalization.DateTimeStyles.AssumeUniversal);
                DateTime reqDate = DateTime.ParseExact(s_reqDate, "dd.MM.yyyy",
                    System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, System.Globalization.DateTimeStyles.AssumeUniversal);

                if (dateNow.Date > reqDate.Date)
                {
                    //Program.log.Fatal("Срок лицензии истёк");
                    MessageBox.Show("Срок лицензии истёк", "Ошибка активации лицензии", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                return true;
            }
            else return false;
        }
        */



        public MainForm()
        {

            /////////активация//////////////////
            if (!OnStartup())
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
                return;
            }
            ////////////////////////////////////

            Utils._MainForm = this;
            if (!LoadConfig())
            {
                Parameters parameters = new Parameters();
                if (parameters.ShowDialog() == DialogResult.OK)
                {
                    CLIENT_ID = parameters.textClientID.Text;
                    CLIENT_SECRET = parameters.textClientSecret.Text;
                    ACCOUNT_ID = parameters.textAccountID.Text;
                    SaveConfig();
                }
                else
                {
                    Close();
                }
            }
            
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();

            //checkConfig();

            imageList1.Images.Add(Utils.IconExtractor.Extract("shell32.dll", 3, true));
            imageList1.Images.Add(Utils.IconExtractor.Extract("shell32.dll", 1, true));
            imageList1.Images.Add(Utils.IconExtractor.Extract("shell32.dll", 238, true));
            var b = Utils.IconExtractor.Extract("shell32.dll", 268, true).ToBitmap();


            searchLabel.Image = new Bitmap(b, new Size(20, 20));

            //if (Properties.Settings.Default.UPDATE_LIST == null)
            {
                //Properties.Settings.Default.UPDATE_LIST = new System.Collections.Specialized.StringCollection();
                //Properties.Settings.Default.Save();
            }

            dummy = null;
            PopulateTreeView();
            treeView1.AfterExpand += TreeView1_AfterExpand;
            SearchBox.TextChanged += SearchBox_TextChanged;
        }

        string sto = "";

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            string st = SearchBox.Text.ToLower();
            if (st.Contains(sto))
            {
                List<TreeNode> toRemove = new List<TreeNode>();
                foreach (TreeNode t in rootNodeFiltered.Nodes)
                    if (!t.Text.ToLower().Contains(st))
                        toRemove.Add(t);
                foreach (var n in toRemove)
                    rootNodeFiltered.Nodes.Remove(n);
            }
            else
            {
                foreach (TreeNode t in rootNode.Nodes)
                    if (t.Text.ToLower().Contains(st))
                    {
                        if (!rootNodeFiltered.Nodes.Contains(t))
                            rootNodeFiltered.Nodes.Add(t);
                    }
                    else
                    {
                        if (rootNodeFiltered.Nodes.Contains(t))
                            rootNodeFiltered.Nodes.Remove(t);
                    }
            }
            sto = st;
        }

        TreeNode rootNodeFiltered;
        TreeNode rootNode;

        bool populate = false;

        private void PopulateTreeView()
        {
            populate = true;
            void popTree()
            {
                try
                {
                    lock (_ThreadLock)
                    {
                        SearchBox.Enabled = false;
                        rootNodeFiltered = null;
                        if (IsHandleCreated)
                        {
                            // Always synchronous.  (But you must watch out for cross-threading deadlocks!)
                            if (InvokeRequired)
                            {
                                this.Invoke(new InvokeDelegate(() =>
                        {
                            treeView1.Nodes.Clear();
                            listView1.Items.Clear();
                            treeView1.Nodes.Add(new TreeNode("Loading Projects...", 2, 2));
                        }));
                            }
                        }
                        ExplorerItem item = Projects.Get(ACCOUNT_ID);
                        rootNodeFiltered = new TreeNode(item.Name);
                        rootNode = new TreeNode(item.Name);
                        rootNodeFiltered.Tag = item;
                        rootNode.Tag = item;
                        rootNode.Nodes.Add(new TreeNode("Loading...", 2, 2));
                        GetDirectories(rootNode);
                        foreach (TreeNode t in rootNode.Nodes)
                            rootNodeFiltered.Nodes.Add(t);
                        Invoke(new InvokeDelegate(() =>
                        {
                            treeView1.Nodes.Clear();
                            treeView1.Nodes.Add(rootNodeFiltered);
                        }));
                        SearchBox.Enabled = true;
                    }
                }
                
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.StackTrace, ex.Message);
                    Invoke(new InvokeDelegate(() =>
                    {
                        treeView1.Nodes.Clear();
                    }));
                }
                
                populate = false;
            }
            new Task(popTree).Start();
        }

        private void GetDirectories(TreeNode nodeToAddTo)
        {
            lock (_ThreadLock)
            {
                try
                {
                    TreeNode aNode;
                    ExplorerItem item = (ExplorerItem)(nodeToAddTo.Tag);
                    var items = item.getFolders();
                    nodeToAddTo.Nodes.Clear();
                    foreach (ExplorerItem subDir in items)
                    {
                        aNode = new TreeNode(subDir.Name, 0, 0);
                        aNode.Tag = subDir;
                        aNode.ImageKey = "folder";
                        aNode.Nodes.Add(new TreeNode("Loading...", 2, 2));

                        Invoke(new InvokeDelegate(() => nodeToAddTo.Nodes.Add(aNode)));
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.StackTrace, ex.Message);
                }
            }
        }
        

        private void TreeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Nodes[0].Tag == null)
            {
                new Task(() => GetDirectories(e.Node)).Start();
            }
        }
        
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Parent == null)
                return;
            void updateList()
            {
                lock (_ThreadLock)
                {
                    try
                    {
                        listView1.Items.Clear();
                        TreeNode newSelected = e.Node;
                        ExplorerItem item = (ExplorerItem)(newSelected.Tag);
                        if (item == null)
                            return;

                        bool removed = false;
                        var files = item.getFiles();
                        var folders = item.getFolders();
                        foreach (ExplorerItem folder in folders)
                        {
                            var aNode = new ListViewItem(new string[] { folder.Name, folder.Type, folder.Modified }, 0);
                            aNode.Tag = folder;
                            /*
                            var tmp = Properties.Settings.Default.UPDATE_LIST;
                            aNode.Checked =
                    Properties.Settings.Default.UPDATE_LIST.Contains(
                        $"{folder.project},{folder.Path}"
                        );
                        */
                            listView1.Items.Add(aNode);
                        }

                        foreach (ExplorerItem file in files)
                        {
                            var aNode = new ListViewItem(new string[] { file.Name, file.Type, file.Modified }, 1);
                            aNode.Tag = file;
                            /*
                            aNode.Checked =
                    Properties.Settings.Default.UPDATE_LIST.Contains(
                        $"{file.project},{file.Path}"
                        );
                        */
                            listView1.Items.Add(aNode);
                        }

                        listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show(this, ex.StackTrace, ex.Message);
                    }
                }
            }

            new Task(updateList).Start();
        }

        void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            
        }
        

        ListViewItem selectedItem;
        static ZXing.BarcodeReader r = new BarcodeReader();

        //Encryption Key
        static private byte[] EncryptionKey = Encoding.ASCII.GetBytes("4d89g13j4j91j27c582ji693");
        // The Initialization Vector for the DES encryption routine
        static private byte[] IV = Encoding.ASCII.GetBytes("abhijeet");
        /// Encrypts a text block
        static public string Encrypt(string textToEncrypt)
        {
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = EncryptionKey;
            tdes.IV = IV;
            byte[] buffer = Encoding.ASCII.GetBytes(textToEncrypt);
            return Convert.ToBase64String(tdes.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length));
        }

        /// Decrypts an encrypted text block

        static public string Decrypt(string textToDecrypt)
        {
            byte[] buffer = Convert.FromBase64String(textToDecrypt);
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.Key = EncryptionKey;
            des.IV = IV;
            return Encoding.ASCII.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length));
        }
        void QRAdd(QRLoading load, Folders.Folder d, bool cont = false)
        {
            lock (_ThreadLock)
            {
                string file_id = null;
                string folder_id = null;
                string pdfName = null;
                try
                {
                    string qrLink = "";

                    Versions vers = Versions.Get(d.links.self.href + "/versions");
                    file_id = d.id;
                    if (vers == null)
                        throw new Exception($"File {d.Name} not found in BIM");
                    byte[] fileBytes = null;
                    load.statusLabel.Text = ("Downloading file");
                    load.statusBar.Value = 6;
                    //loadTime.Add(new Tuple<double, string>((DateTime.Now - start).TotalSeconds, "Downloading file"));
                    Thread.Sleep(500);
                    fileBytes = Utils.getBytesURL(vers.data[0].relationships.storage.meta.link.href);
                    qrLink = vers.data[0].links.self.href;
                    int version = int.Parse(qrLink.Substring(qrLink.LastIndexOf('=') + 1));
                    qrLink = qrLink.Substring(0, qrLink.LastIndexOf('=') + 1);
                    qrLink = qrLink + (version + 1);
                    if (File.Exists("tmpPDFFile"))
                        File.Delete("tmpPDFFile");
                    using (var SW = (File.OpenWrite("tmpPDFFile")))
                    {
                        SW.Write(fileBytes, 0, fileBytes.Length);
                    }

                    string mask = "projects/";
                    qrLink = qrLink.Substring(qrLink.IndexOf(mask) + mask.Length) + ';' +
                        CLIENT_ID + ';' +
                       CLIENT_SECRET;

                    load.statusLabel.Text = ("Updating file");
                    load.statusBar.Value = 20;
                    //loadTime.Add(new Tuple<double, string>((DateTime.Now - start).TotalSeconds, "Updating file"));
                    Thread.Sleep(500);
                    int imagesReplaced = 0;
                    pdfName = d.Name;
                    if (File.Exists(pdfName))
                        File.Delete(pdfName);
                    using (PdfReader pdf = new PdfReader("tmpPDFFile"))
                    {
                        PdfStamper stp = new PdfStamper(pdf, new FileStream(pdfName, FileMode.Create));
                        PdfWriter writer = stp.Writer;
                        Bitmap imgb = null;
                        if (qrLink == "")
                        {
                            imgb = new Bitmap(100, 100);
                            for (int i = 0; i < imgb.Width; i++)
                                for (int j = 0; j < imgb.Width; j++)
                                {
                                    var color = Color.Black;
                                    if (i == 0 || j == 0 || i == imgb.Width - 1 || j == imgb.Width - 1)
                                        color = Color.Blue;
                                    imgb.SetPixel(i, j, color);
                                }
                        }
                        else
                        {
                            qrLink = "@" + Encrypt(qrLink);
                            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                            BitMatrix bitMatrix = multiFormatWriter.encode(qrLink, BarcodeFormat.QR_CODE, 500, 500);
                            ZXing.BarcodeWriter barcodeEncoder = new BarcodeWriter();
                            imgb = barcodeEncoder.Write(bitMatrix);
                        }
                        imgb.Save("image.png");
                        imgb.Dispose();
                        int cnt = 0;
                        for (int i = 1; i <= pdf.NumberOfPages; i++)
                        {
                            PdfDictionary pg = pdf.GetPageN(i);
                            PdfDictionary res =
                              (PdfDictionary)PdfReader.GetPdfObject(pg.Get(PdfName.RESOURCES));
                            PdfDictionary xobj =
                              (PdfDictionary)PdfReader.GetPdfObject(res.Get(PdfName.XOBJECT));
                            if (xobj != null)
                            {
                                foreach (PdfName name in xobj.Keys)
                                {
                                    PdfObject obj = xobj.Get(name);
                                    if (obj.IsIndirect() && PdfReader.GetPdfObject(obj) is PdfDictionary)
                                    {

                                        PdfDictionary tg = (PdfDictionary)PdfReader.GetPdfObject(obj);
                                        
                                        PdfName type =
                                          (PdfName)PdfReader.GetPdfObject(tg.Get(PdfName.SUBTYPE));
                                        if (PdfName.IMAGE.Equals(type))
                                        {
                                            byte[] bytes = PdfReader.GetStreamBytesRaw(((PRStream)PdfReader.GetPdfObject(obj)));
                                            if ((bytes != null))
                                            {
                                                MemoryStream ms = new MemoryStream(bytes);
                                                try
                                                {
                                                    System.Drawing.Image imgPDF = System.Drawing.Image.FromStream(ms);
                                                    var bitmap = new Bitmap(imgPDF);
                                                    var code = r.Decode(bitmap);
                                                    bitmap.Dispose();
                                                    imgPDF.Dispose();
                                                    if (code != null && code.Text == @"https://www.pss.spb.ru/")
                                                    {
                                                        PdfReader.KillIndirect(obj);
                                                        iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance("image.png");
                                                        iTextSharp.text.Image maskImage = img.ImageMask;
                                                        if (maskImage != null)
                                                            writer.AddDirectImageSimple(maskImage);
                                                        writer.AddDirectImageSimple(img, (PRIndirectReference)obj);
                                                        imagesReplaced++;
                                                    }
                                                }
                                                catch(Exception ex) { }

                                            }

                                        }
                                    }
                                }
                            }
                        }
                        load.statusLabel.Text = ("Codes replaced: " + imagesReplaced);
                        load.statusBar.Value = 27;

                        Thread.Sleep(500);
                        try { File.Delete("image.png"); }
                        catch { }
                        try { File.Delete("tmpimg.png"); }
                        catch { }
                        stp.Close();
                    }
                    if (imagesReplaced > 0)
                    {
                        load.statusLabel.Text = ("Uploading file");
                        load.statusBar.Value = 32;
                        Thread.Sleep(500);
                        ((Folders.Folder)d.parent).UploadFile(load, ((Projects.Project)d.project).id, d.id, pdfName, file_id);

                        load.statusLabel.Text = ($"{imagesReplaced} codes replaced in file {pdfName}.");
                        load.statusBar.Value = 100;
                    }
                    else
                    {
                        load.statusLabel.Text = ("File is already updated");
                        load.statusBar.Value = 100;
                        //MessageBox.Show(this, $"File {pdfName} has been already updated.\n");
                    }
                    if (File.Exists("tmpPDFFile"))
                        try
                        {
                            File.Delete("tmpPDFFile");
                        }
                        catch
                        {
                            MessageBox.Show("Failed to remove temp file");
                        }
                    if (File.Exists(pdfName))
                        try
                        {
                            File.Delete(pdfName);
                        }
                        catch
                        {
                            MessageBox.Show("Failed to remove updated file");
                        }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.StackTrace, ex.Message);
                }
                if (!cont)
                {
                    load.statusCancel.Text = "Close";
                    load.CancelButton = load.statusCancel;
                    load.statusCancel.Enabled = true;
                }
            }
        }

        void QRAddMult(QRLoadingMult load, List<Folders.Folder> files)
        {
            lock (_ThreadLock)
            {
                int i = 0;
                load.indexBar.Value = 0;
                files = files.FindAll(x => Path.GetExtension(x.Name)?.ToLower() == ".pdf");
                foreach (var f in files)
                {
                    i++;
                    load.indexLabel.Text = $"PDF Files: {i}/{files.Count}";
                    QRAdd(load, f, true);
                    load.indexBar.Value = 100 * (i) / files.Count;
                    Thread.Sleep(1000);
                }
                load.statusLabel.Text = "Done";
                load.statusCancel.Text = "Close";
                load.CancelButton = load.statusCancel;
                load.statusCancel.Enabled = true;
            }
        }

        void GetAllFolders(QRLoading load, List<Folders.Folder> FilesList, ExplorerItem F)
        {
            lock (_ThreadLock)
            {
                
                load.Text = $"Files Found: {FilesList.Count}";
                foreach (var f in F.getFolders())
                    GetAllFolders(load, FilesList, f);

                foreach (var f in F.getFiles())
                {
                    FilesList.Add((Folders.Folder)f);
                }
            }
            
        }

        private void addQRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (((Folders.Folder)selectedItem.Tag).IsFolder)
            {
                //MessageBox.Show(this, "Cant add QR to folder", "Error");
                //return;
                List<Folders.Folder> FilesList = new List<Folders.Folder>();
                using (QRLoading load = new QRLoading())
                {
                    load.Text = "Waiting for other tasks to complete";
                    load.statusBar.Style = ProgressBarStyle.Marquee;
                    load.statusCancel.Enabled = false;
                    new Task(() => { GetAllFolders(load, FilesList, (ExplorerItem)selectedItem.Tag); load.Close(); }).Start();
                    load.ShowDialog(this);
                }

                using (QRLoadingMult load = new QRLoadingMult())
                {
                    new Task(() => QRAddMult(load, FilesList)).Start();
                    load.ShowDialog(this);
                }
            }
            else
            {
                QRLoading load = new QRLoading();
                load.Text = "Waiting for other tasks to complete";
                Folders.Folder d = ((Folders.Folder)selectedItem.Tag);
                new Task(() => QRAdd(load, d)).Start();
                load.ShowDialog(this);
            }
        }

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
           if (e.Button == MouseButtons.Right)
            {
                if (listView1.FocusedItem.Bounds.Contains(e.Location))
                {
                    selectedItem = listView1.FocusedItem;
                    listViewContext.Show(Cursor.Position);
                }
            }
        }
        
        private void listView1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            /*
            ExplorerItem item = listView1.Items[e.Index].Tag as ExplorerItem;
            var tmp = Properties.Settings.Default.UPDATE_LIST;
            if (e.NewValue == CheckState.Checked)
            {
                if (!Properties.Settings.Default.UPDATE_LIST.Contains(
                    $"{item.project},{item.Path}"
                    ))
                {
                    Properties.Settings.Default.UPDATE_LIST.Add(
                        $"{item.project},{item.Path}"
                        );
                    Properties.Settings.Default.Save();
                }
            }
            else
            {
                Properties.Settings.Default.UPDATE_LIST.Remove(
                    $"{item.project},{item.Path}"
                    );
                Properties.Settings.Default.Save();
            }
            */
        }

        private void configToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (populate)
            {
                MessageBox.Show(this, "Wait for loading to complete", "Error");
                return;
            }
            using (Parameters par = new Parameters())
            {
                par.textClientID.Text = CLIENT_ID;
                par.textClientSecret.Text = CLIENT_SECRET;
                par.textAccountID.Text = ACCOUNT_ID;
                var res = par.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    CLIENT_ID = par.textClientID.Text;
                    CLIENT_SECRET = par.textClientSecret.Text;
                    ACCOUNT_ID = par.textAccountID.Text;
                    SaveConfig();
                    
                    Utils.token = null;
                    PopulateTreeView();
                }
            }
        }

        private void parsingToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
