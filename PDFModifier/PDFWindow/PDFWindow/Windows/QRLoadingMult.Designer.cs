﻿namespace PDFWindow.Windows
{
    partial class QRLoadingMult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.indexLabel = new System.Windows.Forms.Label();
            this.indexBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(9, 84);
            // 
            // statusCancel
            // 
            this.statusCancel.Location = new System.Drawing.Point(97, 124);
            // 
            // statusLabel
            // 
            this.statusLabel.Location = new System.Drawing.Point(10, 63);
            // 
            // indexLabel
            // 
            this.indexLabel.AutoSize = true;
            this.indexLabel.Location = new System.Drawing.Point(10, 9);
            this.indexLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.indexLabel.Name = "indexLabel";
            this.indexLabel.Size = new System.Drawing.Size(0, 13);
            this.indexLabel.TabIndex = 4;
            // 
            // indexBar
            // 
            this.indexBar.Location = new System.Drawing.Point(9, 30);
            this.indexBar.Margin = new System.Windows.Forms.Padding(2);
            this.indexBar.Name = "indexBar";
            this.indexBar.Size = new System.Drawing.Size(248, 21);
            this.indexBar.TabIndex = 3;
            // 
            // QRLoadingMult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(268, 162);
            this.Controls.Add(this.indexLabel);
            this.Controls.Add(this.indexBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "QRLoadingMult";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "QRLoadingMult";
            this.Controls.SetChildIndex(this.statusBar, 0);
            this.Controls.SetChildIndex(this.statusCancel, 0);
            this.Controls.SetChildIndex(this.statusLabel, 0);
            this.Controls.SetChildIndex(this.indexBar, 0);
            this.Controls.SetChildIndex(this.indexLabel, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label indexLabel;
        public System.Windows.Forms.ProgressBar indexBar;
    }
}