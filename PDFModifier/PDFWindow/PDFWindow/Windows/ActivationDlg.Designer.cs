﻿namespace PSS_360VerScan.Settings
{
    partial class ActivationDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActivationDlg));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label_serial_number = new System.Windows.Forms.Label();
            this.label_request_code = new System.Windows.Forms.Label();
            this.label_activation_code = new System.Windows.Forms.Label();
            this.textBox_serial_number = new System.Windows.Forms.TextBox();
            this.textBox_activation_code = new System.Windows.Forms.TextBox();
            this.textBox_request_code = new System.Windows.Forms.TextBox();
            this.button_activate = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.button_cancel = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label_serial_number, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label_request_code, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label_activation_code, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox_serial_number, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox_activation_code, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox_request_code, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.870368F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.46297F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.407407F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.92592F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.870368F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.46297F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(314, 241);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // label_serial_number
            // 
            this.label_serial_number.AutoSize = true;
            this.label_serial_number.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_serial_number.Location = new System.Drawing.Point(3, 0);
            this.label_serial_number.Name = "label_serial_number";
            this.label_serial_number.Size = new System.Drawing.Size(90, 15);
            this.label_serial_number.TabIndex = 0;
            this.label_serial_number.Text = "Serial Number:";
            // 
            // label_request_code
            // 
            this.label_request_code.AutoSize = true;
            this.label_request_code.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_request_code.Location = new System.Drawing.Point(3, 79);
            this.label_request_code.Name = "label_request_code";
            this.label_request_code.Size = new System.Drawing.Size(88, 15);
            this.label_request_code.TabIndex = 1;
            this.label_request_code.Text = "Request Code:";
            // 
            // label_activation_code
            // 
            this.label_activation_code.AutoSize = true;
            this.label_activation_code.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_activation_code.Location = new System.Drawing.Point(3, 158);
            this.label_activation_code.Name = "label_activation_code";
            this.label_activation_code.Size = new System.Drawing.Size(93, 15);
            this.label_activation_code.TabIndex = 2;
            this.label_activation_code.Text = "Activation Code:";
            // 
            // textBox_serial_number
            // 
            this.textBox_serial_number.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_serial_number.Location = new System.Drawing.Point(3, 21);
            this.textBox_serial_number.Name = "textBox_serial_number";
            this.textBox_serial_number.Size = new System.Drawing.Size(308, 21);
            this.textBox_serial_number.TabIndex = 8;
            this.textBox_serial_number.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
            // 
            // textBox_activation_code
            // 
            this.textBox_activation_code.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_activation_code.Location = new System.Drawing.Point(3, 179);
            this.textBox_activation_code.Name = "textBox_activation_code";
            this.textBox_activation_code.Size = new System.Drawing.Size(308, 21);
            this.textBox_activation_code.TabIndex = 9;
            this.textBox_activation_code.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
            // 
            // textBox_request_code
            // 
            this.textBox_request_code.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox_request_code.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_request_code.Location = new System.Drawing.Point(3, 99);
            this.textBox_request_code.Name = "textBox_request_code";
            this.textBox_request_code.Size = new System.Drawing.Size(308, 21);
            this.textBox_request_code.TabIndex = 10;
            this.textBox_request_code.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
            // 
            // button_activate
            // 
            this.button_activate.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_activate.Location = new System.Drawing.Point(3, 3);
            this.button_activate.Name = "button_activate";
            this.button_activate.Size = new System.Drawing.Size(87, 23);
            this.button_activate.TabIndex = 1;
            this.button_activate.Text = "Активировать";
            this.button_activate.UseVisualStyleBackColor = true;
            this.button_activate.Click += new System.EventHandler(this.button_activate_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(96, 3);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(95, 23);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(197, 3);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(89, 23);
            this.button_cancel.TabIndex = 3;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button_activate);
            this.flowLayoutPanel1.Controls.Add(this.splitter1);
            this.flowLayoutPanel1.Controls.Add(this.button_cancel);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(27, 283);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(299, 44);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // ActivationDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 337);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ActivationDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Активация";
            this.Load += new System.EventHandler(this.ActivationDlg_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label_serial_number;
        private System.Windows.Forms.Label label_request_code;
        private System.Windows.Forms.Label label_activation_code;
        private System.Windows.Forms.TextBox textBox_serial_number;
        private System.Windows.Forms.TextBox textBox_activation_code;
        private System.Windows.Forms.TextBox textBox_request_code;
        private System.Windows.Forms.Button button_activate;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}