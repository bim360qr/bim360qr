﻿using DSCryptor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace PSS_360VerScan.Settings
{
    public partial class ActivationDlg : Form
    {
        string macAddress = ActivateHelper.getMacAddress();
        string serialNumber = ActivateHelper.getRegKey("SN");
        //string Date = ActivateHelper.getDate();

        string APP_NAME;

        public ActivationDlg(string app_name)
        {            
            InitializeComponent();
            APP_NAME = app_name;
            this.Text = APP_NAME + ". Активация";
            label_serial_number.Text = "Серийный номер:";
            label_request_code.Text = "Код запроса:";
            label_activation_code.Text = "Код активации:";

#if DEMO_VERSION
            serialNumber = "DEMO_VERSION";
            textBox_serial_number.Enabled = false;
#elif DEMO_VERSION_60
            serialNumber = "DEMO_VERSION_60";
            textBox_serial_number.Enabled = false;
#elif BETA_VERSION
            serialNumber = "BETA_VERSION";
            textBox_serial_number.Enabled = false;
#else
#endif
        }

        private void button_activate_Click(object sender, EventArgs e)
        {
            string ActivationCode = textBox_activation_code.Text;

            //Зашифровываем MAC-адрес и версию в ActivationKey
            CryptoRijndael cryptoActKey_a = new CryptoRijndael("GoodDayTodayPSS360VerScan{C2ED9983-1BA9-44F0-A284-C28BC13CC802}");
            CryptoRijndael cryptoActKey = new CryptoRijndael(serialNumber);//("GoodDayTodayPSS360Backup{C2ED9983-1BA9-44F0-A284-C28BC13CC802}");

            var macAndDate = String.Empty;
            string macAddress = ActivateHelper.getMacAddress();

            string acCodeAndDate = "";
            if (ActivationCode != null)
            {

                try
                {
                    DSCryptor.CryptoRijndael decryptoRCtoMAC = new DSCryptor.CryptoRijndael("GoodDayTodayPSS360VerScan{C2ED9983-1BA9-44F0-A284-C28BC13CC802}");
                    acCodeAndDate = decryptoRCtoMAC.Decrypt(ActivationCode);
                    //acCodeAndDate = decryptoRCtoMAC.Decrypt(acCodeAndDate);
                }
                catch (Exception ex)
                {
                    this.DialogResult = DialogResult.None;
                    MessageBox.Show("Ошибка проверки кода активации", APP_NAME, System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            string[] mac_and_date = acCodeAndDate.Split('&');
            string s_reqDate;
            if (mac_and_date.Length == 2)
            {
                s_reqDate = mac_and_date[1];
            }
            else
            {
                this.DialogResult = DialogResult.None;
                MessageBox.Show("Ошибка проверки кода активации", APP_NAME, System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            
            string activationKey = cryptoActKey.Encrypt(macAddress);
            macAndDate = activationKey + "&" + s_reqDate;
            string activationCode = cryptoActKey_a.Encrypt(macAndDate);

            if (ActivationCode == activationCode)
            {
#if DEMO_VERSION || DEMO_VERSION_60 || BETA_VERSION
                MessageBox.Show("Пробная версия активирована", "Активация", System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Information);
#else
                MessageBox.Show("Лицензия активирована", "Активация", System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Information);
#endif
                //Запись в реестр 
                string userRoot = "HKEY_LOCAL_MACHINE";
                string subkey = @"SOFTWARE\Wow6432Node\PSS GRAITEC\PSS 360VerScan\";
                string keyName = userRoot + "\\" + subkey;
                
                try
                {
                    Registry.SetValue(keyName, "AC", ActivationCode, RegistryValueKind.ExpandString);
                }
                catch (Exception ex_)
                {
                    MessageBox.Show($"Ошибка. Недостаточно прав доступа для активации приложения '{APP_NAME}'." +
                        $"\nЗапустите приложение от имени Администратора. В дальнейшем использовании приложения повышенные права доступа не потребуются." +
                        $"\n\nError: {ex_.Message}", APP_NAME, System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.DialogResult = DialogResult.None;
                    return;
                }
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                this.DialogResult = DialogResult.None;
                MessageBox.Show("Неверный код активации", APP_NAME, System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ActivationDlg_Load(object sender, EventArgs e)
        {
            //Получение RequestCode из serialNumber и MAC-адреса
            //macAddress = ActivateHelper.getMacAddress();
            //revitVersion = ActivateHelper.getRevitVersion();
            //serialNumber = ActivateHelper.getRegKey("SN");

            CryptoRijndael client = new CryptoRijndael(serialNumber);

            string macAddres = ActivateHelper.getMacAddress();       

            string activationKey = client.Encrypt(macAddres);
            string activationKey_and_data = activationKey + "&" + ActivateHelper.getDatePlusYear();
            string requestCode = client.Encrypt(activationKey_and_data);

            textBox_serial_number.Text = serialNumber;
            textBox_request_code.Text = requestCode;

            //CryptoRijndael decryptoRCtoMAC = new CryptoRijndael(serialNumber);
            //string activationCodeAndDate = decryptoRCtoMAC.Decrypt(requestCode);
            //MessageBox.Show( activationCodeAndDate, "activationCodeAndDate");
        }

        private void regRequestCode(string rc)
        {
            RegistryKey localmachine32 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
            RegistryKey localmachine64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);

            RegistryKey FlatAccounting_32 = localmachine32.OpenSubKey(@"SOFTWARE\PSS GRAITEC\PSS 360VerScan\");
            RegistryKey FlatAccounting_64 = localmachine64.OpenSubKey(@"SOFTWARE\Wow6432Node\PSS GRAITEC\PSS 360VerScan");

            string key32 = (string)FlatAccounting_32.GetValue("SN");
            string key64 = (string)FlatAccounting_64.GetValue("SN");

            string key = key32 == null ? key64 : key32;

            Registry.SetValue(key, "RC", rc, RegistryValueKind.ExpandString);
        }



        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (e.KeyData == (Keys.Control | Keys.A))
            {
                textBox.SelectAll();
                e.Handled = e.SuppressKeyPress = true;
            }
        }
    }
}
