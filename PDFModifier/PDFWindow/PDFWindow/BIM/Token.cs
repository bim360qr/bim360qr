﻿using PDFWindow.BIM;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDFWindow
{
    [DataContract]
    public class Token
    {


        [DataMember]
        public string access_token { get; set; }
        [DataMember]
        public string token_type { get; set; }
        [DataMember]
        public int expires_in { get; set; }
        [DataMember]
        public DateTime endTime { get; set; }

        public static DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Token));


        public static Token Get(string scope, string FORGE_CLIENT_ID, string FORGE_CLIENT_SECRET)
        {
            
            string url = "https://developer.api.autodesk.com/authentication/v1/authenticate";
            while (true)
            {
                try
                {
                    using (var webClient = new WebClient())
                    {
                        webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                        var pars = new NameValueCollection();
                        pars.Add("client_id", FORGE_CLIENT_ID);
                        pars.Add("client_secret", FORGE_CLIENT_SECRET);
                        pars.Add("scope", scope);
                        pars.Add("grant_type", "client_credentials");
                        byte[] response = null;
                        response = webClient.UploadValues(url, pars);

                        url = System.Text.Encoding.UTF8.GetString(response);
                    }
                    using (var stream = Utils.GenerateStreamFromString(url))
                    {
                        Token res = (Token)ser.ReadObject(stream);
                        res.endTime = DateTime.Now + new TimeSpan(0, 59, 0);
                        return res;
                    }
                }
                catch (WebException ex)
                {
                    if (ex.Message.Contains("401") || ex.Message.Contains("403"))
                        throw new Exception(ex.Message);
                    else
                        MessageBox.Show(Utils._MainForm, ex.StackTrace, ex.Message);
                    //Console.Title = "!" + DateTime.Now + "#" + ex.Message;
                }
            }
        }

    }
}
