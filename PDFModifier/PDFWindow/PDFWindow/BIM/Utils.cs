﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDFWindow.BIM
{
    static class Utils
    {
        public static Token token;

        public static Form _MainForm = null;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static void checkToken()
        {
            if (token != null && token.endTime > DateTime.Now) return;
            token = Token.Get("data:create data:read data:write", MainForm.CLIENT_ID, MainForm.CLIENT_SECRET);
        }
        public static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        public static string getByURL(string url)
        {
            while (true)
            {
                try
                {
                    checkToken();
                    using (var webClient = new WebClient())
                    {
                        webClient.Headers.Add("Authorization", token.token_type + ' ' + token.access_token);
                        return System.Text.Encoding.UTF8.GetString(webClient.DownloadData(url));
                    }
                }

                catch (Exception ex)
                {
                    if (ex.Message.Contains("401") || ex.Message.Contains("403") || ex.Message.Contains("404"))
                        throw new Exception("ACCOUNT_ID not valid");
                    MessageBox.Show(Utils._MainForm, ex.StackTrace, ex.Message);
                    //Console.Title = "!" + DateTime.Now + "#" + ex.Message;
                    Thread.Sleep(1000);
                }
            }
        }

        public static byte[] getBytesURL(string url)
        {
            while (true)
            {
                try
                {
                    checkToken();
                    using (var webClient = new WebClient())
                    {
                        webClient.Headers.Add("Authorization", token.token_type + ' ' + token.access_token);
                        return webClient.DownloadData(url);
                    }
                }

                catch (Exception ex)
                {
                    if (ex.Message.Contains("401") || ex.Message.Contains("403") || ex.Message.Contains("404"))
                        throw new Exception("ACCOUNT_ID not valid");
                    Console.Title = "!" + DateTime.Now + "#" + ex.Message;
                    Thread.Sleep(1000);
                }
            }
        }

        public class IconExtractor
        {
            public static Icon Extract(string file, int number, bool largeIcon)
            {
                IntPtr large;
                IntPtr small;
                ExtractIconEx(file, number, out large, out small, 1);
                try
                {
                    return Icon.FromHandle(largeIcon ? large : small);
                }
                catch
                {
                    return null;
                }

            }
            [DllImport("Shell32.dll", EntryPoint = "ExtractIconExW", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            private static extern int ExtractIconEx(string sFile, int iIndex, out IntPtr piLargeVersion, out IntPtr piSmallVersion, int amountIcons);

        }

    }
}
