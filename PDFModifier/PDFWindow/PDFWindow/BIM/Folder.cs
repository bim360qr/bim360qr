﻿using PDFWindow.BIM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDFWindow
{
    public interface ExplorerItem
    {
        List<ExplorerItem> getFiles();
        List<ExplorerItem> getFolders();
        string Name { get; }
        bool IsFolder { get; }
        string Type { get; }
        string Modified { get; }
        ExplorerItem parent { get; set; }
        ExplorerItem project { get; set; }
        string Path { get; set; }
    }
    [DataContract] public class Folders
    {
        [DataContract]
        public class Folder:ExplorerItem
        {
            [DataMember] public Relationships2 relationships { get; set; }
            [DataMember] public Attributes2 attributes { get; set; }
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
            [DataMember] public Links13 links { get; set; }

            public string Name => (attributes.name != null) ? attributes.name : attributes.displayName;

            public bool IsFolder => attributes.name != null;

            public string Type => attributes.extension.type;

            public string Modified => attributes.lastModifiedTime.Split('T')[0];

            public ExplorerItem parent { get; set; }
            public ExplorerItem project { get; set; }

            public override string ToString()
            {   
                return attributes.name;
            }

            List<ExplorerItem> items = null;
            List<ExplorerItem> files = null;
            List<ExplorerItem> folders = null;

            List<ExplorerItem> ExplorerItem.getFiles()
            {
                if (files == null)
                    update();
                return files;
            }
            public string Path { get; set; }

            List<ExplorerItem> ExplorerItem.getFolders()
            {
                if (folders == null)
                    update();
                return folders;
            }
            void update()
            {
                items = Get(links.self.href + "/contents").data.ToList<ExplorerItem>();
                foreach (var i in items)
                {
                    i.parent = this;
                    i.project = ((ExplorerItem)this).project;
                    i.Path = System.IO.Path.Combine(Path, i.Name);
                }
                folders = items.FindAll(i => i.IsFolder);
                files = items.FindAll(i => !i.IsFolder);
                
            }

            public void UploadFile(QRLoading load,string project, string folder_id, string name, string item_id)
            {
                StorageRequest SR = new StorageRequest()
                {
                    jsonapi = new StorageRequest.Jsonapi() { version = "1.0" },
                    data = new StorageRequest.Data()
                    {
                        type = "objects",
                        attributes = new StorageRequest.Data.Attributes() { name = name },
                        relationships = new StorageRequest.Data.Relatoinships()
                        {
                            target = new StorageRequest.Data.Relatoinships.Target()
                            {
                                data = new StorageRequest.Data.Relatoinships.Target.TargetData()
                                {
                                    type = "folders",
                                    id = folder_id
                                }
                            }
                        }
                    }

                };
                string responce = null;
                load.statusLabel.Text = ("Creating new bim360 item");
                load.statusBar.Value = 37;
                Thread.Sleep(500);
                while (true)
                {
                    try
                    {
                        Utils.checkToken();
                        using (var webClient = new WebClient())
                        {
                            webClient.Headers.Add("Authorization", Utils.token.token_type + ' ' + Utils.token.access_token);
                            webClient.Headers.Add("Content-Type", "application/vnd.api+json");
                            webClient.Headers.Add("Accept", "application/vnd.api+json");
                            using (Stream S = new MemoryStream())
                            {
                                StorageRequest.ser.WriteObject(S, SR);
                                S.Position = 0;
                                byte[] buffer = new byte[S.Length];
                                S.Read(buffer, 0, (int)S.Length);
                                responce = UTF8Encoding.UTF8.GetString(webClient.UploadData("https://developer.api.autodesk.com/data/v1/projects/" + project + "/storage", buffer));
                            }
                        }
                        break;
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("401") || ex.Message.Contains("403") || ex.Message.Contains("404"))
                            throw new Exception("ACCOUNT_ID not valid");
                        throw;
                        MessageBox.Show(Utils._MainForm, ex.StackTrace, ex.Message);
                        //Console.Title = "!" + DateTime.Now + "#" + ex.Message;
                        Thread.Sleep(1000);
                    }
                }
                using (var stream = Utils.GenerateStreamFromString(responce))
                {
                    SR = (StorageRequest)StorageRequest.ser.ReadObject(stream);
                }
                var V = SR.data.id.Split('/');
                string bucket_id = V[0];
                string file_id = V[1];
                using (Stream S = File.OpenRead(name))
                {
                    load.statusLabel.Text = ("Reading updated file");
                    load.statusBar.Value = 48;

                    Thread.Sleep(500);
                    byte[] buffer = new byte[S.Length];
                    S.Read(buffer, 0, (int)S.Length);
                    load.statusLabel.Text = ("Uploading file into new item");
                    load.statusBar.Value = 52;

                    Thread.Sleep(500);
                    while (true)
                    {
                        try
                        {
                            Utils.checkToken();
                            using (var webClient = new WebClient())
                            {
                                webClient.Headers.Add("Authorization", Utils.token.token_type + ' ' + Utils.token.access_token);
                                responce = UTF8Encoding.UTF8.GetString(webClient.UploadData("https://developer.api.autodesk.com/oss/v2/buckets/wip.dm.prod/objects/" + file_id, "PUT", buffer));
                            }
                            break;
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("401") || ex.Message.Contains("403") || ex.Message.Contains("404"))
                                throw new Exception("ACCOUNT_ID not valid");
                            throw;
                            //MessageBox.Show(Utils.MainForm, ex.StackTrace, ex.Message);
                            //Console.Title = "!" + DateTime.Now + "#" + ex.Message;
                            Thread.Sleep(1000);
                        }
                    }
                }

                StorageReply SRep = null;
                using (var stream = Utils.GenerateStreamFromString(responce))
                {
                    SRep = (StorageReply)StorageReply.ser.ReadObject(stream);
                }
                load.statusLabel.Text = ("Adding new version");
                load.statusBar.Value = 75;

                Thread.Sleep(500);
                while (true)
                {
                    try
                    {
                        Utils.checkToken();
                        using (var webClient = new WebClient())
                        {
                            webClient.Headers.Add("Authorization", Utils.token.token_type + ' ' + Utils.token.access_token);
                            webClient.Headers.Add("Content-Type", "application/vnd.api+json");
                            webClient.Headers.Add("Accept", "application/vnd.api+json");
                            using (Stream S = new MemoryStream())
                            {
                                StorageRequest.ser.WriteObject(S, SR);
                                S.Position = 0;
                                byte[] buffer = new byte[S.Length];
                                S.Read(buffer, 0, (int)S.Length);
                                var tmp = @"{""jsonapi"":{""version"":""1.0""},""data"":{""type"":""versions"",""attributes"":{""name"":""*name*"",""extension"":{""type"":""versions:autodesk.bim360:File"",""version"":""1.0""}},""relationships"":{""item"":{""data"":{""type"":""items"",""id"":""*item_id*""}},""storage"":{""data"":{""type"":""objects"",""id"":""*file_id*""}}}}}";// Encoding.UTF8.GetString(buffer).Replace("\\/","/");
                                tmp = tmp.Replace("*name*", name);
                                tmp = tmp.Replace("*item_id*", item_id);
                                tmp = tmp.Replace("*file_id*", SR.data.id);
                                responce = UTF8Encoding.UTF8.GetString(webClient.UploadData("https://developer.api.autodesk.com/data/v1/projects/" + project + "/versions", "POST", Encoding.UTF8.GetBytes(tmp)));
                            }
                        }
                        break;
                    }
                    catch (WebException ex)
                    {
                        //var err = getErrorStr(ex);
                        if (ex.Message.Contains("401") || ex.Message.Contains("403") || ex.Message.Contains("404"))
                            throw new Exception("ACCOUNT_ID not valid");
                        throw;
                        Console.Title = "!" + DateTime.Now + "#" + ex.Message;
                        Thread.Sleep(1000);
                    }
                }
                using (var stream = Utils.GenerateStreamFromString(responce))
                {
                    SR = (StorageRequest)StorageRequest.ser.ReadObject(stream);
                }
            }

            [DataContract]
            public class StorageRequest
            {
                [DataContract]
                public class Jsonapi
                {
                    [DataMember] public string version { get; set; }
                }

                [DataContract]
                public class Data
                {
                    [DataMember(EmitDefaultValue = false)] public string type { get; set; }
                    [DataMember(EmitDefaultValue = false)] public string id { get; set; }
                    [DataContract]
                    public class Attributes
                    {
                        [DataContract]
                        public class Extension
                        {
                            [DataMember(EmitDefaultValue = false)] public string type { get; set; }
                            [DataMember(EmitDefaultValue = false)] public string version { get; set; }
                        }
                        [DataMember(EmitDefaultValue = false)] public string name { get; set; }
                        [DataMember(EmitDefaultValue = false)] public Extension extension { get; set; }
                    }
                    [DataMember(EmitDefaultValue = false)] public Attributes attributes { get; set; }
                    [DataContract]
                    public class Relatoinships
                    {
                        [DataContract]
                        public class Target
                        {
                            [DataContract]
                            public class TargetData
                            {
                                [DataMember(EmitDefaultValue = false)] public string type { get; set; }
                                [DataMember(EmitDefaultValue = false)] public string id { get; set; }
                            }
                            [DataMember(EmitDefaultValue = false)] public TargetData data { get; set; }
                            [DataContract]
                            public class Links
                            {
                                [DataContract]
                                public class Related
                                {
                                    [DataMember(EmitDefaultValue = false)] public string href { get; set; }
                                }
                                [DataMember(EmitDefaultValue = false)] public Related related { get; set; }
                            }
                            [DataMember(EmitDefaultValue = false)] public Links links { get; set; }
                        }
                        [DataMember(EmitDefaultValue = false)] public Target target { get; set; }
                        [DataMember(EmitDefaultValue = false)] public Target item { get; set; }
                        [DataMember(EmitDefaultValue = false)] public Target storage { get; set; }
                    }
                    [DataMember(EmitDefaultValue = false)] public Relatoinships relationships { get; set; }

                }

                [DataMember(EmitDefaultValue = false)] public Jsonapi jsonapi { get; set; }
                [DataMember(EmitDefaultValue = false)] public Data data { get; set; }
                public static DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(StorageRequest));

            }
            [DataContract]
            class StorageReply
            {
                [DataMember] public string bucketKey { get; set; }
                [DataMember] public string objectId { get; set; }
                [DataMember] public string objectKey { get; set; }
                [DataMember] public string sha1 { get; set; }
                [DataMember] public string size { get; set; }
                [DataMember] public string contentType { get; set; }
                [DataMember] public string location { get; set; }
                public static DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(StorageReply));
            }

        }

        [DataMember] public List<Included> included { get; set; }
        [DataMember] public List<Folder> data { get; set; }
        [DataMember] public Jsonapi jsonapi { get; set; }
        [DataMember] public Links14 links { get; set; }

        public static DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Folders));
        
        public static Folders Get(string url)
        {
            string responce = Utils.getByURL(url);
            using (var stream = Utils.GenerateStreamFromString(responce))
            {
                return (Folders)ser.ReadObject(stream);
            }
        }
        public Folders Navigate(string subFolder)
        {
            foreach (var d in data)
                if (d.attributes.name == subFolder)
                {
                    return Folders.Get(d.links.self.href + "/contents");
                }
            throw new Exception("Folder " + subFolder + " does not exist");
        }
        

        [DataContract] public class Data
        {
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
        }

        [DataContract] public class Related
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links
        {
            [DataMember] public Related related { get; set; }
        }

        [DataContract] public class Item
        {
            [DataMember] public Data data { get; set; }
            [DataMember] public Links links { get; set; }
        }

        [DataContract] public class Self
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Related2
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links2
        {
            [DataMember] public Self self { get; set; }
            [DataMember] public Related2 related { get; set; }
        }

        [DataContract] public class Refs
        {
            [DataMember] public Links2 links { get; set; }
        }

        [DataContract] public class Link
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Meta
        {
            [DataMember] public Link link { get; set; }
        }

        [DataContract] public class Data2
        {
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
        }

        [DataContract] public class Storage
        {
            [DataMember] public Meta meta { get; set; }
            [DataMember] public Data2 data { get; set; }
        }

        [DataContract] public class Self2
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links4
        {
            [DataMember] public Self2 self { get; set; }
        }

        [DataContract] public class Links3
        {
            [DataMember] public Links4 links { get; set; }
        }

        [DataContract] public class Relationships
        {
            [DataMember] public Item item { get; set; }
            [DataMember] public Refs refs { get; set; }
            [DataMember] public Storage storage { get; set; }
            [DataMember] public Links3 links { get; set; }
        }

        [DataContract] public class Properties
        {
        }

        [DataContract] public class Data3
        {
            [DataMember] public object tempUrn { get; set; }
            [DataMember] public string storageType { get; set; }
            [DataMember] public Properties properties { get; set; }
            [DataMember] public string storageUrn { get; set; }
        }

        [DataContract] public class Schema
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Extension
        {
            [DataMember] public Data3 data { get; set; }
            [DataMember] public string version { get; set; }
            [DataMember] public string type { get; set; }
            [DataMember] public Schema schema { get; set; }
        }

        [DataContract] public class Attributes
        {
            [DataMember] public string mimeType { get; set; }
            [DataMember] public string displayName { get; set; }
            [DataMember] public string name { get; set; }
            [DataMember] public Extension extension { get; set; }
            [DataMember] public string createUserName { get; set; }
            [DataMember] public string createTime { get; set; }
            [DataMember] public string createUserId { get; set; }
            [DataMember] public string lastModifiedUserName { get; set; }
            [DataMember] public string lastModifiedUserId { get; set; }
            [DataMember] public int versionNumber { get; set; }
            [DataMember] public string lastModifiedTime { get; set; }
        }

        [DataContract] public class Self3
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links5
        {
            [DataMember] public Self3 self { get; set; }
        }

        [DataContract] public class Included
        {
            [DataMember] public Relationships relationships { get; set; }
            [DataMember] public Attributes attributes { get; set; }
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
            [DataMember] public Links5 links { get; set; }
        }

        [DataContract] public class Self4
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Related3
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links6
        {
            [DataMember] public Self4 self { get; set; }
            [DataMember] public Related3 related { get; set; }
        }

        [DataContract] public class Refs2
        {
            [DataMember] public Links6 links { get; set; }
        }

        [DataContract] public class Self5
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links8
        {
            [DataMember] public Self5 self { get; set; }
        }

        [DataContract] public class Links7
        {
            [DataMember] public Links8 links { get; set; }
        }

        [DataContract] public class Data4
        {
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
        }

        [DataContract] public class Related4
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links9
        {
            [DataMember] public Related4 related { get; set; }
        }

        [DataContract] public class Parent
        {
            [DataMember] public Data4 data { get; set; }
            [DataMember] public Links9 links { get; set; }
        }

        [DataContract] public class Related5
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links10
        {
            [DataMember] public Related5 related { get; set; }
        }

        [DataContract] public class Contents
        {
            [DataMember] public Links10 links { get; set; }
        }

        [DataContract] public class Data5
        {
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
        }

        [DataContract] public class Related6
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links11
        {
            [DataMember] public Related6 related { get; set; }
        }

        [DataContract] public class Tip
        {
            [DataMember] public Data5 data { get; set; }
            [DataMember] public Links11 links { get; set; }
        }

        [DataContract] public class Related7
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links12
        {
            [DataMember] public Related7 related { get; set; }
        }

        [DataContract] public class Versions
        {
            [DataMember] public Links12 links { get; set; }
        }

        [DataContract] public class Relationships2
        {
            [DataMember] public Refs2 refs { get; set; }
            [DataMember] public Links7 links { get; set; }
            [DataMember] public Parent parent { get; set; }
            [DataMember] public Contents contents { get; set; }
            [DataMember] public Tip tip { get; set; }
            [DataMember] public Versions versions { get; set; }
        }

        [DataContract] public class Data6
        {
            [DataMember] public List<string> visibleTypes { get; set; }
            [DataMember] public List<string> allowedTypes { get; set; }
        }

        [DataContract] public class Schema2
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Extension2
        {
            [DataMember] public Data6 data { get; set; }
            [DataMember] public string version { get; set; }
            [DataMember] public string type { get; set; }
            [DataMember] public Schema2 schema { get; set; }
        }

        [DataContract] public class Attributes2
        {
            [DataMember] public string displayName { get; set; }
            [DataMember] public string name { get; set; }
            [DataMember] public Extension2 extension { get; set; }
            [DataMember] public string createUserName { get; set; }
            [DataMember] public int objectCount { get; set; }
            [DataMember] public string createUserId { get; set; }
            [DataMember] public string lastModifiedUserName { get; set; }
            [DataMember] public string lastModifiedUserId { get; set; }
            [DataMember] public string lastModifiedTime { get; set; }
            [DataMember] public bool hidden { get; set; }
            [DataMember] public string path { get; set; }
            [DataMember] public string createTime { get; set; }
            [DataMember] public bool? reserved { get; set; }
            [DataMember] public string reservedUserName { get; set; }
            [DataMember] public string reservedUserId { get; set; }
            [DataMember] public string reservedTime { get; set; }
        }

        [DataContract] public class Self6
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links13
        {
            [DataMember] public Self6 self { get; set; }
        }


        [DataContract] public class Jsonapi
        {
            [DataMember] public string version { get; set; }
        }

        [DataContract] public class Next
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Self7
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Prev
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class First
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links14
        {
            [DataMember] public Next next { get; set; }
            [DataMember] public Self7 self { get; set; }
            [DataMember] public Prev prev { get; set; }
            [DataMember] public First first { get; set; }
        }
        
    }
}
