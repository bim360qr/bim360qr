﻿using PDFWindow.BIM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace PDFWindow
{
    [DataContract] public class Versions
    {
        public static DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Versions));

        public static Versions Get(string url)
        {
            string responce = Utils.getByURL(url);
            using (var stream = Utils.GenerateStreamFromString(responce))
            {
                return (Versions)ser.ReadObject(stream);
            }
        }
        
        [DataMember] public Jsonapi jsonapi { get; set; }
        [DataMember] public Links links { get; set; }
        [DataMember] public Datum[] data { get; set; }
        

        [DataContract] public class Jsonapi
        {
            [DataMember] public string version { get; set; }
        }

        [DataContract] public class Links
        {
            [DataMember] public Self self { get; set; }
        }

        [DataContract] public class Self
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Datum
        {
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
            [DataMember] public Attributes attributes { get; set; }
            [DataMember] public Links1 links { get; set; }
            [DataMember] public Relationships relationships { get; set; }
        }

        [DataContract] public class Attributes
        {
            [DataMember] public string name { get; set; }
            [DataMember] public string displayName { get; set; }
            [DataMember] public string createTime { get; set; }
            [DataMember] public string createUserId { get; set; }
            [DataMember] public string createUserName { get; set; }
            [DataMember] public string lastModifiedTime { get; set; }
            [DataMember] public string lastModifiedUserId { get; set; }
            [DataMember] public string lastModifiedUserName { get; set; }
            [DataMember] public int versionNumber { get; set; }
            [DataMember] public string fileType { get; set; }
            [DataMember] public Extension extension { get; set; }
        }

        [DataContract] public class Extension
        {
            [DataMember] public string type { get; set; }
            [DataMember] public string version { get; set; }
            [DataMember] public Schema schema { get; set; }
            [DataMember] public Data data { get; set; }
        }

        [DataContract] public class Schema
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Data
        {
            [DataMember] public string processState { get; set; }
            [DataMember] public string extractionState { get; set; }
            [DataMember] public string splittingState { get; set; }
            [DataMember] public string reviewState { get; set; }
            [DataMember] public string revisionDisplayLabel { get; set; }
            [DataMember] public string sourceFileName { get; set; }
        }

        [DataContract] public class Links1
        {
            [DataMember] public Self1 self { get; set; }
        }

        [DataContract] public class Self1
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Relationships
        {
            [DataMember] public Item item { get; set; }
            [DataMember] public Links3 links { get; set; }
            [DataMember] public Refs refs { get; set; }
            [DataMember] public Downloadformats downloadFormats { get; set; }
            [DataMember] public Derivatives derivatives { get; set; }
            [DataMember] public Thumbnails thumbnails { get; set; }
            [DataMember] public Storage storage { get; set; }
        }

        [DataContract] public class Item
        {
            [DataMember] public Data1 data { get; set; }
            [DataMember] public Links2 links { get; set; }
        }

        [DataContract] public class Data1
        {
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
        }

        [DataContract] public class Links2
        {
            [DataMember] public Related related { get; set; }
        }

        [DataContract] public class Related
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Links3
        {
            [DataMember] public Links4 links { get; set; }
        }

        [DataContract] public class Links4
        {
            [DataMember] public Self2 self { get; set; }
        }

        [DataContract] public class Self2
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Refs
        {
            [DataMember] public Links5 links { get; set; }
        }

        [DataContract] public class Links5
        {
            [DataMember] public Self3 self { get; set; }
            [DataMember] public Related1 related { get; set; }
        }

        [DataContract] public class Self3
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Related1
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Downloadformats
        {
            [DataMember] public Links6 links { get; set; }
        }

        [DataContract] public class Links6
        {
            [DataMember] public Related2 related { get; set; }
        }

        [DataContract] public class Related2
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Derivatives
        {
            [DataMember] public Data2 data { get; set; }
            [DataMember] public Meta meta { get; set; }
        }

        [DataContract] public class Data2
        {
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
        }

        [DataContract] public class Meta
        {
            [DataMember] public Link link { get; set; }
        }

        [DataContract] public class Link
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Thumbnails
        {
            [DataMember] public Data3 data { get; set; }
            [DataMember] public Meta1 meta { get; set; }
        }

        [DataContract] public class Data3
        {
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
        }

        [DataContract] public class Meta1
        {
            [DataMember] public Link1 link { get; set; }
        }

        [DataContract] public class Link1
        {
            [DataMember] public string href { get; set; }
        }

        [DataContract] public class Storage
        {
            [DataMember] public Data4 data { get; set; }
            [DataMember] public Meta2 meta { get; set; }
        }

        [DataContract] public class Data4
        {
            [DataMember] public string type { get; set; }
            [DataMember] public string id { get; set; }
        }

        [DataContract] public class Meta2
        {
            [DataMember] public Link2 link { get; set; }
        }

        [DataContract] public class Link2
        {
            [DataMember] public string href { get; set; }
        }

    }
}
