﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DSCryptor;

namespace PSS_360VerScan_Activator
{
    public partial class ActivatorForm : Form
    {
        public ActivatorForm()
        {
            InitializeComponent();
            label1.Visible = false;
        }

        private void btGenerateSN_Click(object sender, EventArgs e)
        {
            // Генерация серийного номера
            Guid g = Guid.NewGuid();
            tbSN.Text = g.ToString().ToUpper();
            tbClientSN.Text = g.ToString().ToUpper();
        }

        private void btGenerateKey_Click(object sender, EventArgs e)
        {
            if (tbClientSN.Text == "" || tbRC.Text == "")
            {
                MessageBox.Show("Для генерации ключа активации необходимы\nсерийный номер и код запроса.","Внимание!");
                return;
            }
            string serialNumber = tbClientSN.Text;
            string RequestCode = tbRC.Text;

            //Зная SerialNumber, расшифровываем RequestCode и получаем MAC-адрес клиента и версию
            //string macAddressAndDate = "";
            string activationCodeAndDate = "";
            //string DateNow = "";
            try
            {
                CryptoRijndael decryptoRCtoMAC = new CryptoRijndael(serialNumber);
                activationCodeAndDate = decryptoRCtoMAC.Decrypt(RequestCode);
                //DateNow = DateTime.Now.Date.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Невозможно дешифровать ключи");
                return;
            }

            string[] mac_and_date = activationCodeAndDate.Split('&');


            string reqDate;
            if (mac_and_date.Length == 2)
            {
                reqDate = mac_and_date[1];
            }
            else
            {
                MessageBox.Show("Невозможно дешифровать ключи");
                return;
            }

            //reqDate = "20/09/2019";

            DateTime dt_reqDate = DateTime.ParseExact(reqDate, "dd/MM/yyyy",
                System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, System.Globalization.DateTimeStyles.AssumeUniversal);


            if (DateTime.Now > dt_reqDate.Date)
            {
                MessageBox.Show($"Срок лицензии истёк. Дата активации: {dt_reqDate.Date.ToShortDateString()}");//С начала активации продукта прошло больше года.");
                return;
            }



            label1.Text = "Срок жизни кода кода запроса до " + dt_reqDate.ToShortDateString();//"Лицензия активна до " + dt_reqDate.ToShortDateString();
            label1.Visible = true;


            //Зашифровываем MAC-адрес и версию в ActivationKey
            CryptoRijndael cryptoActKey = new CryptoRijndael("GoodDayTodayPSS360VerScan{C2ED9983-1BA9-44F0-A284-C28BC13CC802}");
            string activationKey = mac_and_date[0]; //cryptoActKey.Encrypt(mac_and_date[0]);
            string new_activationCodeAndDate = activationKey + "&" + getDateFormPicker();
            string activationCode = cryptoActKey.Encrypt(new_activationCodeAndDate);

            CryptoRijndael decryptoRCtoMAC_ = new CryptoRijndael("GoodDayTodayPSS360VerScan{C2ED9983-1BA9-44F0-A284-C28BC13CC802}");
            string _activationCodeAndDate = decryptoRCtoMAC_.Decrypt(activationCode);

            tbKey.Text = activationCode;
        }

        public string getDateFormPicker()
        {
            DateTime date = dateTimePicker1.Value;
            string Date = date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat);
            //string Date = date.ToShortDateString();
            return Date;
        }

        
    }



   
}
