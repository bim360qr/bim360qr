﻿namespace PSS_360VerScan_Activator
{
    partial class ActivatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActivatorForm));
            this.btGenerateSN = new System.Windows.Forms.Button();
            this.btGenerateKey = new System.Windows.Forms.Button();
            this.tbSN = new System.Windows.Forms.TextBox();
            this.tbKey = new System.Windows.Forms.TextBox();
            this.tbRC = new System.Windows.Forms.TextBox();
            this.lbRC = new System.Windows.Forms.Label();
            this.tbClientSN = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbGenerateSN = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // btGenerateSN
            // 
            this.btGenerateSN.Location = new System.Drawing.Point(304, 10);
            this.btGenerateSN.Margin = new System.Windows.Forms.Padding(4);
            this.btGenerateSN.Name = "btGenerateSN";
            this.btGenerateSN.Size = new System.Drawing.Size(124, 28);
            this.btGenerateSN.TabIndex = 0;
            this.btGenerateSN.Text = "Генерировать";
            this.btGenerateSN.UseVisualStyleBackColor = true;
            this.btGenerateSN.Click += new System.EventHandler(this.btGenerateSN_Click);
            // 
            // btGenerateKey
            // 
            this.btGenerateKey.Location = new System.Drawing.Point(304, 250);
            this.btGenerateKey.Margin = new System.Windows.Forms.Padding(4);
            this.btGenerateKey.Name = "btGenerateKey";
            this.btGenerateKey.Size = new System.Drawing.Size(124, 28);
            this.btGenerateKey.TabIndex = 1;
            this.btGenerateKey.Text = "Генерировать";
            this.btGenerateKey.UseVisualStyleBackColor = true;
            this.btGenerateKey.Click += new System.EventHandler(this.btGenerateKey_Click);
            // 
            // tbSN
            // 
            this.tbSN.Location = new System.Drawing.Point(16, 42);
            this.tbSN.Margin = new System.Windows.Forms.Padding(4);
            this.tbSN.Name = "tbSN";
            this.tbSN.ReadOnly = true;
            this.tbSN.Size = new System.Drawing.Size(411, 22);
            this.tbSN.TabIndex = 2;
            // 
            // tbKey
            // 
            this.tbKey.Location = new System.Drawing.Point(16, 286);
            this.tbKey.Margin = new System.Windows.Forms.Padding(4);
            this.tbKey.Name = "tbKey";
            this.tbKey.Size = new System.Drawing.Size(411, 22);
            this.tbKey.TabIndex = 3;
            // 
            // tbRC
            // 
            this.tbRC.Location = new System.Drawing.Point(16, 162);
            this.tbRC.Margin = new System.Windows.Forms.Padding(4);
            this.tbRC.Name = "tbRC";
            this.tbRC.Size = new System.Drawing.Size(411, 22);
            this.tbRC.TabIndex = 4;
            // 
            // lbRC
            // 
            this.lbRC.AutoSize = true;
            this.lbRC.Location = new System.Drawing.Point(12, 143);
            this.lbRC.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbRC.Name = "lbRC";
            this.lbRC.Size = new System.Drawing.Size(351, 17);
            this.lbRC.TabIndex = 5;
            this.lbRC.Text = "Введите сюда код запроса, полученный от клиента";
            // 
            // tbClientSN
            // 
            this.tbClientSN.Location = new System.Drawing.Point(16, 102);
            this.tbClientSN.Margin = new System.Windows.Forms.Padding(4);
            this.tbClientSN.Name = "tbClientSN";
            this.tbClientSN.Size = new System.Drawing.Size(411, 22);
            this.tbClientSN.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 82);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(272, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Введите сюда серийный номер клиента";
            // 
            // lbGenerateSN
            // 
            this.lbGenerateSN.AutoSize = true;
            this.lbGenerateSN.Location = new System.Drawing.Point(12, 22);
            this.lbGenerateSN.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbGenerateSN.Name = "lbGenerateSN";
            this.lbGenerateSN.Size = new System.Drawing.Size(120, 17);
            this.lbGenerateSN.TabIndex = 11;
            this.lbGenerateSN.Text = "Серийный номер";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 266);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Ключ активации";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.Location = new System.Drawing.Point(233, 191);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker1.TabIndex = 13;
            // 
            // ActivatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 331);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbGenerateSN);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbClientSN);
            this.Controls.Add(this.lbRC);
            this.Controls.Add(this.tbRC);
            this.Controls.Add(this.tbKey);
            this.Controls.Add(this.tbSN);
            this.Controls.Add(this.btGenerateKey);
            this.Controls.Add(this.btGenerateSN);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ActivatorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PSS 360Backup. Активатор";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btGenerateSN;
        private System.Windows.Forms.Button btGenerateKey;
        private System.Windows.Forms.TextBox tbSN;
        private System.Windows.Forms.TextBox tbKey;
        private System.Windows.Forms.TextBox tbRC;
        private System.Windows.Forms.Label lbRC;
        private System.Windows.Forms.TextBox tbClientSN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbGenerateSN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}

