package pss.graitek.verscan;

import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Key;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class HttpHandler {

    private static final String TAG = HttpHandler.class.getSimpleName();

    public HttpHandler() {
    }
    static TripleDES enc=new TripleDES("4d89g13j4j91j27c582ji693","abhijeet");

    public String makeServiceCall(String reqUrl) {
        String response = null;
        String token="";
        URL url = null;
        HttpURLConnection conn=null;
        try {
            if (reqUrl.startsWith("@"))
            {
                reqUrl=reqUrl.substring(1);
                reqUrl = enc.decryptText(reqUrl);
                if(reqUrl == null){
                    return "Wrong QR";
                }
            }
            String[] values=reqUrl.split(";");
            if(values.length!=3){
                return "Wrong QR";
            }
            url = new URL("https://developer.api.autodesk.com/authentication/v1/authenticate");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");

            conn.setDoOutput(true);
            OutputStreamWriter writer =
                    new OutputStreamWriter(conn.getOutputStream());
            writer.write("client_id="+values[1]+"&"+
                    "client_secret="+values[2]+"&"+
                    "scope=data:read data:write&"+
                    "grant_type=client_credentials");
            writer.flush();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

            JSONObject jsonObj = new JSONObject(response);
            token=jsonObj.getString("access_token");
            url = new URL("https://developer.api.autodesk.com/data/v1/projects/"+values[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.addRequestProperty("Authorization","Bearer "+token);

            in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
            jsonObj = new JSONObject(response).getJSONObject("data");

            final int currentVersion=jsonObj.getJSONObject("attributes")
                    .getInt("versionNumber");
            String date=jsonObj.getJSONObject("attributes")
                    .getString("lastModifiedTime");
            date=date.substring(0,date.indexOf('T'));
            final String name=jsonObj.getJSONObject("attributes")
                    .getString("name");
            final String finalDate = date;
            /*
                txtResult.post(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    public void run() {
                        txtResult.setText(name + "\n" +
                                "Version number: " + currentVersion + "\n" +
                                "Creation date: " + finalDate + "\n" +
                                "Processing...");
                    }
                });
                */
            response= jsonObj.getJSONObject("relationships")
                    .getJSONObject("item")
                    .getJSONObject("data")
                    .getString("id");
            url = new URL("https://developer.api.autodesk.com/data/v1/projects/b.7471a1a6-0326-4c30-a396-b3e0481b0539/items/" + response + "/versions");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.addRequestProperty("Authorization","Bearer "+token);

            in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
            jsonObj = new JSONObject(response);
            final JSONArray versions = jsonObj.getJSONArray("data");
            response="Current version: "+currentVersion+"\nVersions in DOCS360:\n";
            int maxVer=0;
            int maxVerIndex=0;

            for (int i = 0; i < versions.length(); i++) {
                JSONObject c = versions.getJSONObject(i);
                int ver=c.getJSONObject("attributes")
                        .getInt("versionNumber");
                if(ver>maxVer)
                {
                    maxVer=ver;
                    maxVerIndex=i;
                }
            }
            if(maxVer>currentVersion) {
                return "false;"+currentVersion+";"+maxVer;
                /*
                final int finalMaxVer = maxVer;
                final int finalMaxVerIndex = maxVerIndex;
                txtResult.post(new Runnable() {
                    public void run() {
                        int colorFrom = Color.rgb(255,255,255);
                        int colorTo = Color.rgb(255,200,200);
                        final ObjectAnimator backgroundColorAnimator = ObjectAnimator.ofObject(txtResult,
                                "backgroundColor",
                                new ArgbEvaluator(),
                                0xFFFFFFFF,
                                0xffffc8c8);
                        backgroundColorAnimator.setDuration(1000);
                        backgroundColorAnimator.start();
                        //txtResult.setBackgroundColor(Color.rgb(255,200,200));
                        String old= (String) txtResult.getText();
                        old=old.replace("Processing...","");
                        try {
                            String date=versions.getJSONObject(finalMaxVerIndex).getJSONObject("attributes")
                                    .getString("lastModifiedTime");
                            date=date.substring(0,date.indexOf('T'));
                            txtResult.setText(old+"\n" +
                                    "Newest version is: "+ finalMaxVer+"\n" +
                                    "Creation date: "+date);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                */
            }
            else {
                return "true;" + currentVersion + ";" + maxVer;
                /*
                txtResult.post(new Runnable() {
                    public void run() {
                        final ObjectAnimator backgroundColorAnimator = ObjectAnimator.ofObject(txtResult,
                                "backgroundColor",
                                new ArgbEvaluator(),
                                0xFFFFFFFF,
                                0xffc8ffc8);
                        backgroundColorAnimator.setDuration(1000);
                        backgroundColorAnimator.start();
                        String old = (String) txtResult.getText();
                        old = old.substring(0, old.lastIndexOf('\n'));
                        txtResult.setText(old + "\n\n" +
                                "Current version is newest");
                    }
                });
                */
            }
            //response+=	Build.VERSION.SDK_INT;
        } catch (final Exception e) {
            return "error";
            /*
            response = e.getMessage();
            final String errorString="";
            try {
                response=convertStreamToString(conn.getErrorStream());
            }
            catch (Exception ex)
            {

            }
            txtResult.post(new Runnable() {
                public void run() {
                    String old= (String) txtResult.getText();
                    old=old.replace("Processing...","");
                    txtResult.setText(old+"\n" +
                            "Error\n" +
                            e.getMessage()+"\n"+
                            errorString);
                }
            });
            */
        }
        //return response;
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }
}
