package pss.graitek.verscan;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;

public class DataActivity extends AppCompatActivity  {
    final DataActivity activity = this;
    TextSwitcher txtResult;
    TextSwitcher txtSubResult;
    LinearLayout frame;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_data);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Animation inAnimation = AnimationUtils.loadAnimation(this,
                android.R.anim.fade_in);
        inAnimation.setDuration(2500);
        Animation inAnimation2 = AnimationUtils.loadAnimation(this,
                android.R.anim.fade_in);
        inAnimation2.setDuration(3500);

        txtResult = (TextSwitcher) findViewById(R.id.txtResult);
        //txtResult.setFactory(this);
        txtResult.setInAnimation(inAnimation);
        //txtResult.setOutAnimation(outAnimation);

        txtSubResult = (TextSwitcher) findViewById(R.id.txtSubResult);

        //txtSubResult.setFactory(this);

        txtSubResult.setInAnimation(inAnimation2);
        //txtSubResult.setOutAnimation(outAnimation);


        frame = (LinearLayout) findViewById(R.id.main_frame);

        txtResult.post(new Runnable() {
            @Override
            public void run() {
                Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(100);
                new GetContacts().execute(getIntent().getStringExtra("QR_DATA"));
            }
        });
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    public View makeView() {
        TextView textView = new TextView(this);
        textView.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL);
        textView.setTextSize(70);
        textView.setTextColor(Color.RED);
        return textView;
    }

    private class GetContacts extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            txtResult.setCurrentText(getString(R.string.please_wait));
        }

        @Override
        protected Void doInBackground(String... arg0) {
            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            final String jsonStr = sh.makeServiceCall(arg0[0]);

            if (jsonStr=="Wrong QR")
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtResult.setText(getString(R.string.QR_error));
                    }
                });
            }
            else if(jsonStr=="error")
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtResult.setText(getString(R.string.error));
                    }
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtSubResult.setText(getString(R.string.error_hint));
                    }
                });
            }
            else
            {
                final String[] res = jsonStr.split(";");
                if(Boolean.parseBoolean(res[0]))
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            final ObjectAnimator backgroundColorAnimator = ObjectAnimator.ofObject(frame,
                                    "backgroundColor",
                                    new ArgbEvaluator(),
                                    0xFFFFFFFF,
                                    0xffd0ffd0);
                            backgroundColorAnimator.setDuration(1000);
                            backgroundColorAnimator.start();
                        }
                    });
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtResult.setText(getString(R.string.success));
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtSubResult.setText(getString(R.string.success_sub));
                        }
                    });
                }
                else
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            final ObjectAnimator backgroundColorAnimator = ObjectAnimator.ofObject(frame,
                                    "backgroundColor",
                                    new ArgbEvaluator(),
                                    0xFFFFFFFF,
                                    0xffffd0d0);
                            backgroundColorAnimator.setDuration(1000);
                            backgroundColorAnimator.start();
                        }
                    });
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtResult.setText(getString(R.string.fail)+res[1]);
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtSubResult.setText(getString(R.string.fail_sub)+res[2]);
                        }
                    });
                }
            }
            return null;
        }

    }
}
